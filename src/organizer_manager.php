<?php
require_once 'bootstrap.php';

if (!isUserLoggedIn() || !isset($_GET["action"]) || getLoggedType() != "amministratore") {
    header("location: login.php");
}

$templateParams["notifiche"] = $dbh->getUnreadNotifications($_SESSION["email"]);
$templateParams["categorie"] = $dbh->getCategories();



if ($_GET["action"] == 1) {
    // organizzatori attivi
    $val = 1;
    $result = $dbh->getActivatedOrganizer($val);
    $templateParams["organizzatori"] = $result;
    $templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/executive_utils.js");
    $templateParams["pagina"] = "organizer_page.php";
}

if ($_GET["action"] == 2) {
    // clienti non attivi
    $val = 0;
    $result = $dbh->getActivatedOrganizer($val);
    $templateParams["organizzatori"] = $result;
    $templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/executive_utils.js");
    $templateParams["pagina"] = "organizer_page.php";
}

require 'template/base.php';
