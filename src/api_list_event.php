<?php
require_once 'bootstrap.php';

$eventi = $dbh->getEventByCategory($_GET["categoria"]);

//function to order
function cmp($a, $b)
{
    return strcmp(strtolower($a[$_GET["order"]]), strtolower($b[$_GET["order"]]));
}

    //calculating events to be showed 
    $itemPerPage = $_GET["itemPerPage"];
    if (isset($_GET["pagina"])) {
        $page = $_GET["pagina"];

        //computing offsets
        if ($page == "1") {
            $end = $itemPerPage;
            $start = 0;
        } else {
            $end = $page * $itemPerPage;
            $start = ($page - 1) * $itemPerPage;
        }

        //checking remaining events 
        if ($end > count($eventi)) {
            //in this case last page is reached
            $end = count($eventi);
        }

        //removing events not to be showed
        array_splice($eventi, 0, $start);
        array_splice($eventi, $end, count($eventi));

        //adjusting index values
        $end -= $start;
        $start = 0;

        //sorting
        usort($eventi, "cmp");
        header('Content-Type: application/json');
        echo json_encode($eventi);
    }
?>