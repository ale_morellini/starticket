<?php
require_once 'bootstrap.php';
if (isset($_POST["email"]) && isset($_POST["password"])) {
    $result = $dbh->getCredential($_POST["email"]);
    if (count($result) == 0) {
        $templateParams["errorelogin"] = "Errore! Controllare username o password!";
    } else {
        $result = $result[0];
        $hash = $result["pwd"];
        if (password_verify($_POST["password"], $hash)) {
            if ($result["tipologia"] == "cliente") {
                //Login cliente
                $login_result = $dbh->getClibyMail($_POST["email"]);
                if ($login_result[0]["attivo"] != 1) {
                    $templateParams["errorelogin"] = "Il tuo account è stato bloccato";
                } else {
                    registerLoggedUser($login_result[0]);
                }
            }
            if ($result["tipologia"] == "organizzatore") {
                //Login organizzatore
                $login_result = $dbh->getOrgbyMail($_POST["email"]);
                if ($login_result[0]["attivo"] != 1) {
                    $templateParams["errorelogin"] = "Il tuo account non è ancora attivo, verrai contattato via mail dall'amministratore per attivarlo";
                } else {
                    registerLoggedUser($login_result[0]);
                }
            }
            if ($result["tipologia"] == "amministratore") {
                //Login amministratore
                $login_result = $dbh->getAmmbyMail($_POST["email"]);
                registerLoggedUser($login_result[0]);
            }
        } else {
            $templateParams["errorelogin"] = "Errore! Controllare username o password!";
        }
    }
}

$templateParams["categorie"] = $dbh->getCategories();
if (isUserLoggedIn()) {
    $templateParams["notifiche"] = $dbh->getUnreadNotifications($_SESSION["email"]);
} else {
    $templateParams["notifiche"] = "";
}

if (isUserLoggedIn()) {
    if (isset($_GET["cartLogin"])) {
        $templateParams["titolo"] = "Starticket - Checkout";
        header("location:checkout.php");
    } else {
        if ($_SESSION["tipologia"] == "amministratore") {
            header("location: executive_login.php");
        } else {
            if (isset($_GET["action"]) && $_GET["action"] == 1) {
                $templateParams["pagina"] = "login_event.php";
            } else {
                $templateParams["pagina"] = "login_home.php";
            }
        }
    }
} else {
    $templateParams["titolo"] = "Starticket - Login";
    $templateParams["pagina"] = "login_form.php";
}


if (isset($_GET["formmsg"])) {
    $templateParams["formmsg"] = $_GET["formmsg"];
}

require 'template/base.php';
