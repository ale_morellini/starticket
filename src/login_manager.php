<?php
require_once 'bootstrap.php';

if (!isUserLoggedIn()) {
    header("location: login.php");
}

if ($_SESSION["tipologia"] == "organizzatore") {
    $templateParams["info"] = $dbh->getOrgbyMail($_SESSION["email"])[0];
} else {
    $templateParams["info"] = $dbh->getClibyMail($_SESSION["email"])[0];
}

$templateParams["notifiche"] = $dbh->getUnreadNotifications($_SESSION["email"]);
$templateParams["titolo"] = "Starticket - Info manager";
$templateParams["pagina"] = "personal_info_form.php";
$templateParams["categorie"] = $dbh->getCategories();

function checkForm()
{

    if (!isset($_POST["nome"]) || !isset($_POST["cognome"]) || !isset($_POST["indirizzo"]) || !isset($_POST["città"]) || !isset($_POST["cap"]) || !isset($_POST["stato"])) {
        return false;
    }
    return true;
};

if (checkForm()) {
    $hash;
    if ($_POST["password"] != "") {
        $password = $_POST["password"];
        $hash = password_hash($password, PASSWORD_BCRYPT);
        $result = $dbh->updatePwd($hash, $_SESSION["email"]);
    }
    if (isset($_POST["checkbox"])) {
        $result = $dbh->updateNewsletter(1, $_SESSION["email"]);
    } else {
        $result = $dbh->updateNewsletter(0, $_SESSION["email"]);
    }
    if ($_SESSION["tipologia"] == "organizzatore") {
        $result = $dbh->updateOrganizer($_POST["nome"], $_POST["cognome"], $_POST["indirizzo"], $_POST["città"], $_POST["cap"], $_POST["stato"], $_POST["piva"], $_SESSION["email"]);
        $login_result = $dbh->getOrgbyMail($_SESSION["email"]);
        registerLoggedUser($login_result[0]);
    } else {
        $result = $dbh->updateClient($_POST["nome"], $_POST["cognome"], $_POST["indirizzo"], $_POST["città"], $_POST["cap"], $_POST["stato"], $_SESSION["email"]);
        $login_result = $dbh->getClibyMail($_SESSION["email"]);
        registerLoggedUser($login_result[0]);
    }
    header("location: login.php");
}


require 'template/base.php';
