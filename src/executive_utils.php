<?php


require_once 'bootstrap.php';

function stampaRisultato($result)
{
    if ($result == 0) {
        echo "not";
    } else {
        echo "ok";
    }
}

$type = $_POST["type"];

switch ($type) {

    case "trash":
        $id = $_POST["id"];
        $dbh->deleteTickets($id);
        $result = $dbh->deleteEvent($id);
        stampaRisultato($result);
        break;

    case "client":
        $result = $dbh->setClientAttivation($_POST["id"], $_POST["attivo"]);
        stampaRisultato($result);
        break;

    case "organizer":
        $result = $dbh->setOrganizerAttivation($_POST["id"], $_POST["attivo"]);
        stampaRisultato($result);
        break;

    case "addCategory":
        $result = $dbh->insertCategory($_POST["categoria"]);
        stampaRisultato($result);
        break;

    case "deleteCategory":
        $result = $dbh->deleteCategory($_POST["id"]);
        stampaRisultato($result);
        break;

    default:
        echo "nothing done";
}
