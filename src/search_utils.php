<?php

require_once 'bootstrap.php';

if (isset($_GET["ricerca"]) && isset($_GET["pagina"])) {
    $substring = $_GET["ricerca"];
    $page = $_GET["pagina"];
    $itemPerPage = $_GET["itemPerPage"];
    $result = $dbh->getEvent();
    $cercati = array();
    foreach ($result as $evento) {
        $titolo = $evento["titoloevento"];
        $pos = $dbh->locate($titolo, $substring)[0]["pos"];
        if ($pos != 0) {
            array_push($cercati, $evento);
        }
    }
    //computing offsets
    if ($page == "1") {
        $end = $itemPerPage;
        $start = 0;
    } else {
        $end = $page * $itemPerPage;
        $start = ($page - 1) * $itemPerPage;
    }

    //checking remaining events 
    if ($end > count($cercati)) {
        //in this case last page is reached
        $end = count($cercati);
    }

    //removing events not to be showed
    array_splice($cercati, $end);
    array_splice($cercati, 0, $start);

   
    header('Content-Type: application/json');
    echo json_encode($cercati);
}
