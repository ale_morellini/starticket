<?php

require_once 'bootstrap.php';

function stampaRisultato($result)
{
    if ($result == 0) {
        echo "not";
    } else {
        echo "ok";
    }
}

$type = $_POST["type"];

switch ($type) {

    case "trash":
        $id = $_POST["id"];
        $result = $dbh->deleteNotification($id);
        stampaRisultato($result);
        echo $result;
        break;

    case "read":
        $result = $dbh->setNotificationRead($_POST["id"], $_POST["letto"]);
        break;

    default:
        echo "nothing done";
}
