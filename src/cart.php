<?php
require_once 'bootstrap.php';
//Base Template
$templateParams["titolo"] = "Starticket-carrello";
$templateParams["categorie"] = $dbh->getCategories();
$templateParams["pagina"] = "cart_builder.php";
$templateParams["cartEvents"] = array();
$templateParams["js"] = array("js/jquery-3.4.1.min.js","js/cart.js");
if(isUserLoggedIn()){
    $templateParams["notifiche"] = $dbh->getUnreadNotifications($_SESSION["email"]);
}else{
    $templateParams["notifiche"] = "";
}

//Checking cart events added by user
if(!empty($_SESSION["carrello"])){
    $templateParams["cartEvents"] = array();
    foreach ($_SESSION["carrello"] as $event => $quantity) :
        $templateParams["cartEvents"] += [$event=>$quantity];
    endforeach;
}else{
    if(isset($_GET["success"])){
        $templateParams["pagina"] = "checkout_success.php"; 
    }else{
        $templateParams["pagina"] = "cart_empty.php"; 
        unset($templateParams["js"]);
    }
}

require_once 'template/base.php';
?>