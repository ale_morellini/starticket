//Entry point
$(document).ready(function () {

    //Function to icrease/decrease quantity attribute for each event in cart
    function increase(button, condition) {

        const max = button.val();
        let actionDone = false;
        let boxId = 0;
        let boxvalue = 0;
        let temp = button.attr('id');

        //Updating quantity placeholder
        if (condition) {
            boxId = temp.slice(3, temp.length);
            boxvalue = parseInt($("#" + boxId).attr('placeholder'));
            if (boxvalue + 1 <= max) {
                boxvalue += 1;
                $("#" + boxId).attr('placeholder', boxvalue);
                actionDone = true;
            }
        } else {
            boxId = temp.slice(5, temp.length);
            boxvalue = parseInt($("#" + boxId).attr('placeholder'));
            if (boxvalue - 1 > 0) {
                boxvalue -= 1;
                $("#" + boxId).attr('placeholder', boxvalue);
                actionDone = true;
            }
        }

        let val = $("#cost").attr("title")*boxvalue;
        $("#cost").text(parseInt(val));

        //Update event "quantity" also in $_SESSION only if needed
        if (actionDone) {
            $.post("cart_utils.php",
                {
                    type: "updateQuantity",
                    id: boxId,
                    quantity: boxvalue
                }
                ,
                function (data, status) {
                    if (status == "success" && data == "ok"){
                    //In this case is not necessary to display an alert nor to reload page
                    }else{
                        alert("Errore del server!");
                        window.location.reload();
                    }
                });
        }
    }

    function trash(button) {
        let id = button.attr('title');

        $.post("cart_utils.php",
            {
                type: "trash",
                id: parseInt(id)
            }
            ,
            function (data, status) {

                if (status == "success" && data == "ok"){
                    alert("Evento eliminato correttamente dal carrello");
                }else{
                    alert("Errore del server!");
                }
                window.location.reload();
            });
    }

    //Manages checkout operation
    function buy() {
        
        $.post("cart_utils.php",
            {
                type: "buy"
            }
            ,
            function (data, status) {

                if (status == "success" && data == "ok") {
                    window.location = "checkout.php";
                } else if (status == "success" && data == "notLogged") {
                    window.location = "login.php?cartLogin=1";
                }else{
                    window.location.reload();
                }
            });
    }


    //Cart up button
    $(".up").click(function () {
        increase($(this), true);
    });

    //Cart down button
    $(".down").click(function () {
        increase($(this), false);
    });

    //Cart trash button
    $(".trash").click(function () {
        trash($(this));
    });

    //Cart buy button
    $("#buy").click(function () {
        buy();
    });

});



