//This script is responsible for main event carousel view

let index = 0;
let slideOnScreen = 3;
let eventi = 0;

//Shows events on screen
function stampaEventi(eventi){

    gestisciEventi();
    function gestisciEventi(){

        let i;
        for (i = 0; i < slideOnScreen ; i++) {
            if (index<0){
                index=eventi.length-1;
            }
            if(index>(eventi.length-1)){
                index=0;
            }
            let slide = document.getElementById("slide_"+i);
            $("#"+i).attr("href","selected_event.php?event="+eventi[index]["idevento"]);
            $(slide).attr("src",eventi[index]["imgevento"]);
            index++;
        }
        setTimeout(gestisciEventi, 6000);
    }  
}  

//Entry point
//api-evento returns a set of events
$(document).ready(function(){
    $.getJSON("api_event.php", function(data){
        eventi = data;
        stampaEventi(data);
    });

    function nextEvents(direction){
        let i;
        for (i = 0; i < slideOnScreen ; i++) {
            if (index<0){
                index=eventi.length-1;
            }
            if(index>(eventi.length-1)){
                index=0;
            }
            let slide = document.getElementById("slide_"+i);
            $("#"+i).attr("href","selected_event.php?event="+eventi[index]["idevento"]);
            $(slide).attr("src",eventi[index]["imgevento"]);
            if(direction){
                index++
            }else{
                index--;
            }
        }
    }  

    //Carousel button listener
    $("#carosello_right").click(function(){
        nextEvents(true);
    });

    //Carousel button listener
    $("#carosello_left").click(function(){
        nextEvents(false);
    });
});



