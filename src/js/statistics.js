//This scripts is used by amministrator

$(document).ready(function () {

    //Send newsletter button
    $("#sendNewsletter").click(function () {
        sendNews();
    });

    //AJAX. This function send newsletter to users 
    function sendNews() {
        $.post("api_statistics.php", {

            "sendNews": 1

        }, function (data, status) {


            if (status == "success" && data != null) {
                alert("Newsletter inviata correttamente");
            } else {
                alert("Errore nell'esecuzione");
            }
        });
    }

    $("#contactCli").click(function () {

        //hidden value in php page (client mail)
        let email = $("#cliEmail").val();

        //send events in newsLetter
        contactCli(email);

    });

    //AJAX. This function send congrats notification to clients 
    function contactCli(email) {
        $.post("api_statistics.php", {

            "cli": 1,
            "email": email

        }, function (data, status) {

            if (status == "success" && data != null) {
                alert("Messaggio inviato correttamente");
            } else {
                alert("Errore nell'esecuzione");
            }

        });
    }

    $("#contactOrg").click(function () {

        //hidden value in php page (organizer mail)
        let email = $("#orgEmail").val();

        //send events in newsLetter
        contactOrg(email);

    });

    //AJAX. This function send congrats notification to organizer 
    function contactOrg(email) {
        $.post("api_statistics.php", {

            "org": 1,
            "email": email

        }, function (data, status) {

            if (status == "success" && data != null) {
                alert("Messaggio inviato correttamente");
            } else {
                alert("Errore nell'esecuzione");
            }

        });
    }
});

