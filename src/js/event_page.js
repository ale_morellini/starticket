//Entry point
//Script for addToCart button

//AJAX 
$(document).ready(function () {
    $("#addToCart").click(function () {
        $.post("cart_utils.php",
            {
                type: "add",
                id: $("#addToCart").val()
            }
            ,
            function (data, status) {
                if (data > 0 && status == "success" ) {
                    const baseCart = $(".badge > p");
                    baseCart.text(data);
                    window.location = "cart.php";
                    
                }else{
                    alert("Errore del server!");
                    window.location.reload();
                }
            });
    });
});



