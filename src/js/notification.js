function trash(icon) {
    let id = icon.attr('id');

    $.post("notification_utils.php",
        {
            type: "trash",
            id: parseInt(id)
        }
        ,
        function (data, status) {
            window.location.reload();
        });
}

function read(id, letto) {

    $.post("notification_utils.php",
        {
            type: "read",
            id: parseInt(id),
            letto: letto
        }
        ,
        function (data, status) {

        });
}

function stampaNum() {

    $.post("api_notification.php", {

    }, function (data, status) {
        if (status == "success") {
            if (data != 0) {
                $("#num_not").empty();
                $("#num_not").append(data);
            }else{
                $("#num_not").empty();
                $("#num_not").append("");
            }
        }

    });

}

$(document).ready(function () {

    //Event trash button
    $(".trash").click(function () {
        trash($(this));
    });

    $(".link").click(function () {

        let id = $(this).val();
        if ($('#close_' + id).attr("id") != null) {
            read(id, 1);
            $("#close_" + id).remove();
            let icon = `<i id="open_${id}" class="far fa-envelope-open"</i>`
            $("#div_" + id).append(icon);
        }
        stampaNum();
    });
});