function trash(button) {
    let id = button.attr('id');

    $.post("executive_utils.php",
        {
            type: "trash",
            id: parseInt(id)
        }
        ,
        function (data, status) {

            if (status != "success" || data != "ok") {
                alert("Impossibile eliminare l'evento'");
            }
            window.location.reload();
        });
}

function deleteCat(button) {
    let id = button.attr('id');

    $.post("executive_utils.php",
        {
            type: "deleteCategory",
            id: parseInt(id)
        }
        ,
        function (data, status) {

            if (status != "success" || data != "ok") {
                alert("Impossibile eliminare la categoria");
            }
            window.location.reload();
        });
}

function insertCat(input) {
    let value = input.val();
    $.post("executive_utils.php",
        {
            type: "addCategory",
            categoria: value
        }
        ,
        function (data, status) {

            if (status != "success" || data != "ok") {
                alert("Impossibile inserire la categoria");
            }
            window.location.reload();

        });
}

function activated_client(attivo, item) {
    let id = item.attr('id');

    $.post("executive_utils.php",
        {
            type: "client",
            attivo: attivo,
            id: id
        }
        ,
        function (data, status) {

            if (status != "success" || data != "ok") {
                alert("Modifica avvenuta senza successo");
            }
            window.location.reload();
        });
}

function activated_organizer(attivo, item) {
    let id = item.attr('id');

    $.post("executive_utils.php",
        {
            type: "organizer",
            attivo: attivo,
            id: id
        }
        ,
        function (data, status) {

            if (status != "success" || data != "ok") {
                alert("Modifica avvenuta senza successo");
            }
            window.location.reload();
        });
}

$(document).ready(function () {

    //Event trash button
    $(".trash").click(function () {
        trash($(this));
    });

    $("#btnCategoria").click(function () {
        insertCat($("#inputCategoria"));
    });

    $(".trashCat").click(function () {
        deleteCat($(this));
    });

    $(".orgPlus").click(function () {
        activated_organizer(1, $(this));
    });

    $(".orgSlash").click(function () {
        activated_organizer(0, $(this));
    });

    $(".cliPlus").click(function () {
        activated_client(1, $(this));
    });

    $(".cliSlash").click(function () {
        activated_client(0, $(this));
    });
});



