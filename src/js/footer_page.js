//This script builds pages with a sample text. Should
function buildPage(page) {
    let result = "";
    let textPage = "";

    switch(page) {
        case "chi_siamo":
          textPage = `
                  <h3>Chi siamo</h3></br>
                    <h5>La nostra storia</h5>
                    <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dapibus, erat scelerisque egestas tristique, erat lorem condimentum libero, at gravida quam orci vitae ex. Nulla dignissim ipsum lacus. Etiam non dolor et elit porttitor consectetur. Proin in dolor quis nisi malesuada maximus eu condimentum enim. Suspendisse rhoncus, tortor ut lacinia mattis, turpis dui imperdiet elit, id efficitur metus nulla vitae ligula. Proin ut tortor vel ante porttitor volutpat. Pellentesque facilisis tortor vitae turpis elementum, ut varius ante dignissim. Nam vel hendrerit lorem. Vivamus at nulla vestibulum, consequat tortor id, ullamcorper ligula. Suspendisse eu sagittis tellus.
                      </p></br>

                      <h5>Il marchio</h5>
                    <p>
                    Aenean aliquet gravida aliquam. Nunc at porta metus. Cras imperdiet vitae felis a malesuada. Etiam ut purus ultrices dui fringilla congue. Proin sit amet ultricies nibh. Nunc hendrerit euismod elit id laoreet. Nam ac est sit amet enim porta pellentesque id a tortor. Donec id blandit magna. Phasellus gravida a sapien at pulvinar. Fusce vitae interdum ex, id rhoncus lacus. Integer pulvinar aliquet nisi, in luctus lacus molestie ut. Ut eget commodo nibh. Aliquam et eros non sem porttitor pulvinar a quis ex. Vestibulum faucibus congue nisi, eu tincidunt mauris. Phasellus ex eros, tempus non ultrices at, ullamcorper vel nunc.
                      </p></br>

                      <h5>Controllo qualità</h5>
                      <p>
                      Aenean aliquet gravida aliquam. Nunc at porta metus. Cras imperdiet vitae felis a malesuada. Etiam ut purus ultrices dui fringilla congue. Proin sit amet ultricies nibh. Nunc hendrerit euismod elit id laoreet. Nam ac est sit amet enim porta pellentesque id a tortor. Donec id blandit magna. Phasellus gravida a sapien at pulvinar. Fusce vitae interdum ex, id rhoncus lacus. Integer pulvinar aliquet nisi, in luctus lacus molestie ut. Ut eget commodo nibh. Aliquam et eros non sem porttitor pulvinar a quis ex. Vestibulum faucibus congue nisi, eu tincidunt mauris. Phasellus ex eros, tempus non ultrices at, ullamcorper vel nunc.
                        </p>
        `;
          break;
        case "privacy":
          textPage = `
          <h3>Privacy</h3></br>
            <h5>Rispetto della privacy</h5>
            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dapibus, erat scelerisque egestas tristique, erat lorem condimentum libero, at gravida quam orci vitae ex. Nulla dignissim ipsum lacus. Etiam non dolor et elit porttitor consectetur. Proin in dolor quis nisi malesuada maximus eu condimentum enim. Suspendisse rhoncus, tortor ut lacinia mattis, turpis dui imperdiet elit, id efficitur metus nulla vitae ligula. Proin ut tortor vel ante porttitor volutpat. Pellentesque facilisis tortor vitae turpis elementum, ut varius ante dignissim. Nam vel hendrerit lorem. Vivamus at nulla vestibulum, consequat tortor id, ullamcorper ligula. Suspendisse eu sagittis tellus.
              </p></br>

              <h5>Tutela dei dati</h5>
            <p>
            Aenean aliquet gravida aliquam. Nunc at porta metus. Cras imperdiet vitae felis a malesuada. Etiam ut purus ultrices dui fringilla congue. Proin sit amet ultricies nibh. Nunc hendrerit euismod elit id laoreet. Nam ac est sit amet enim porta pellentesque id a tortor. Donec id blandit magna. Phasellus gravida a sapien at pulvinar. Fusce vitae interdum ex, id rhoncus lacus. Integer pulvinar aliquet nisi, in luctus lacus molestie ut. Ut eget commodo nibh. Aliquam et eros non sem porttitor pulvinar a quis ex. Vestibulum faucibus congue nisi, eu tincidunt mauris. Phasellus ex eros, tempus non ultrices at, ullamcorper vel nunc.
              </p></br>

              <h5>Regolamento interno</h5>
              <p>
              Aenean aliquet gravida aliquam. Nunc at porta metus. Cras imperdiet vitae felis a malesuada. Etiam ut purus ultrices dui fringilla congue. Proin sit amet ultricies nibh. Nunc hendrerit euismod elit id laoreet. Nam ac est sit amet enim porta pellentesque id a tortor. Donec id blandit magna. Phasellus gravida a sapien at pulvinar. Fusce vitae interdum ex, id rhoncus lacus. Integer pulvinar aliquet nisi, in luctus lacus molestie ut. Ut eget commodo nibh. Aliquam et eros non sem porttitor pulvinar a quis ex. Vestibulum faucibus congue nisi, eu tincidunt mauris. Phasellus ex eros, tempus non ultrices at, ullamcorper vel nunc.
                </p>
          `;
          break;
        case "cookie":
          textPage = `
          <h3>Cookie</h3></br>
            <h5>Cookie utilizzati</h5>
            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dapibus, erat scelerisque egestas tristique, erat lorem condimentum libero, at gravida quam orci vitae ex. Nulla dignissim ipsum lacus. Etiam non dolor et elit porttitor consectetur. Proin in dolor quis nisi malesuada maximus eu condimentum enim. Suspendisse rhoncus, tortor ut lacinia mattis, turpis dui imperdiet elit, id efficitur metus nulla vitae ligula. Proin ut tortor vel ante porttitor volutpat. Pellentesque facilisis tortor vitae turpis elementum, ut varius ante dignissim. Nam vel hendrerit lorem. Vivamus at nulla vestibulum, consequat tortor id, ullamcorper ligula. Suspendisse eu sagittis tellus.
              </p></br>

              <h5>Regolamento GDPR</h5>
            <p>
            Aenean aliquet gravida aliquam. Nunc at porta metus. Cras imperdiet vitae felis a malesuada. Etiam ut purus ultrices dui fringilla congue. Proin sit amet ultricies nibh. Nunc hendrerit euismod elit id laoreet. Nam ac est sit amet enim porta pellentesque id a tortor. Donec id blandit magna. Phasellus gravida a sapien at pulvinar. Fusce vitae interdum ex, id rhoncus lacus. Integer pulvinar aliquet nisi, in luctus lacus molestie ut. Ut eget commodo nibh. Aliquam et eros non sem porttitor pulvinar a quis ex. Vestibulum faucibus congue nisi, eu tincidunt mauris. Phasellus ex eros, tempus non ultrices at, ullamcorper vel nunc.
              </p></br>

              <h5>Finalità profilazione</h5>
              <p>
              Aenean aliquet gravida aliquam. Nunc at porta metus. Cras imperdiet vitae felis a malesuada. Etiam ut purus ultrices dui fringilla congue. Proin sit amet ultricies nibh. Nunc hendrerit euismod elit id laoreet. Nam ac est sit amet enim porta pellentesque id a tortor. Donec id blandit magna. Phasellus gravida a sapien at pulvinar. Fusce vitae interdum ex, id rhoncus lacus. Integer pulvinar aliquet nisi, in luctus lacus molestie ut. Ut eget commodo nibh. Aliquam et eros non sem porttitor pulvinar a quis ex. Vestibulum faucibus congue nisi, eu tincidunt mauris. Phasellus ex eros, tempus non ultrices at, ullamcorper vel nunc.
                </p>
          `;
        break;
        case "condizioni_generali":
          textPage = `
          <h3>Condizioni generali</h3></br>
            <h5>Utilizzo dei biglietti</h5>
            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dapibus, erat scelerisque egestas tristique, erat lorem condimentum libero, at gravida quam orci vitae ex. Nulla dignissim ipsum lacus. Etiam non dolor et elit porttitor consectetur. Proin in dolor quis nisi malesuada maximus eu condimentum enim. Suspendisse rhoncus, tortor ut lacinia mattis, turpis dui imperdiet elit, id efficitur metus nulla vitae ligula. Proin ut tortor vel ante porttitor volutpat. Pellentesque facilisis tortor vitae turpis elementum, ut varius ante dignissim. Nam vel hendrerit lorem. Vivamus at nulla vestibulum, consequat tortor id, ullamcorper ligula. Suspendisse eu sagittis tellus.
              </p></br>

              <h5>Rimborso</h5>
            <p>
            Aenean aliquet gravida aliquam. Nunc at porta metus. Cras imperdiet vitae felis a malesuada. Etiam ut purus ultrices dui fringilla congue. Proin sit amet ultricies nibh. Nunc hendrerit euismod elit id laoreet. Nam ac est sit amet enim porta pellentesque id a tortor. Donec id blandit magna. Phasellus gravida a sapien at pulvinar. Fusce vitae interdum ex, id rhoncus lacus. Integer pulvinar aliquet nisi, in luctus lacus molestie ut. Ut eget commodo nibh. Aliquam et eros non sem porttitor pulvinar a quis ex. Vestibulum faucibus congue nisi, eu tincidunt mauris. Phasellus ex eros, tempus non ultrices at, ullamcorper vel nunc.
              </p></br>

              <h5>Diritto di recesso</h5>
              <p>
              Aenean aliquet gravida aliquam. Nunc at porta metus. Cras imperdiet vitae felis a malesuada. Etiam ut purus ultrices dui fringilla congue. Proin sit amet ultricies nibh. Nunc hendrerit euismod elit id laoreet. Nam ac est sit amet enim porta pellentesque id a tortor. Donec id blandit magna. Phasellus gravida a sapien at pulvinar. Fusce vitae interdum ex, id rhoncus lacus. Integer pulvinar aliquet nisi, in luctus lacus molestie ut. Ut eget commodo nibh. Aliquam et eros non sem porttitor pulvinar a quis ex. Vestibulum faucibus congue nisi, eu tincidunt mauris. Phasellus ex eros, tempus non ultrices at, ullamcorper vel nunc.
                </p>
          `;
            break;
        case "contatti":
          textPage = `
          <h3>Contatti</h3></br>
            <h5>Domande frequenti</h5>
            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dapibus, erat scelerisque egestas tristique, erat lorem condimentum libero, at gravida quam orci vitae ex. Nulla dignissim ipsum lacus. Etiam non dolor et elit porttitor consectetur. Proin in dolor quis nisi malesuada maximus eu condimentum enim. Suspendisse rhoncus, tortor ut lacinia mattis, turpis dui imperdiet elit, id efficitur metus nulla vitae ligula. Proin ut tortor vel ante porttitor volutpat. Pellentesque facilisis tortor vitae turpis elementum, ut varius ante dignissim. Nam vel hendrerit lorem. Vivamus at nulla vestibulum, consequat tortor id, ullamcorper ligula. Suspendisse eu sagittis tellus.
              </p></br>

              <h5>Call center</h5>
            <p>
            Aenean aliquet gravida aliquam. Nunc at porta metus. Cras imperdiet vitae felis a malesuada. Etiam ut purus ultrices dui fringilla congue. Proin sit amet ultricies nibh. Nunc hendrerit euismod elit id laoreet. Nam ac est sit amet enim porta pellentesque id a tortor. Donec id blandit magna. Phasellus gravida a sapien at pulvinar. Fusce vitae interdum ex, id rhoncus lacus. Integer pulvinar aliquet nisi, in luctus lacus molestie ut. Ut eget commodo nibh. Aliquam et eros non sem porttitor pulvinar a quis ex. Vestibulum faucibus congue nisi, eu tincidunt mauris. Phasellus ex eros, tempus non ultrices at, ullamcorper vel nunc.
            Telefono: 0192345691
              </p></br>

              <h5>Supporto online</h5>
              <p>
              Aenean aliquet gravida aliquam. Nunc at porta metus. Cras imperdiet vitae felis a malesuada. Etiam ut purus ultrices dui fringilla congue. Proin sit amet ultricies nibh. Nunc hendrerit euismod elit id laoreet. Nam ac est sit amet enim porta pellentesque id a tortor. Donec id blandit magna. Phasellus gravida a sapien at pulvinar. Fusce vitae interdum ex, id rhoncus lacus. Integer pulvinar aliquet nisi, in luctus lacus molestie ut. Ut eget commodo nibh. Aliquam et eros non sem porttitor pulvinar a quis ex. Vestibulum faucibus congue nisi, eu tincidunt mauris. Phasellus ex eros, tempus non ultrices at, ullamcorper vel nunc.
                </p>
          `;
          break;
        default:
          textPage = `
          <h3>OOPS! pagina non presente</h3></br>
          `;
      }

      result += textPage;
    return result;
}

$(document).ready(function () {
    
    let text = buildPage($("#goToPage").val());
    let section = $("section");
    section.empty();
    section.append(text);
});

