//Entry point
$(document).ready(function () {

    $('#buy').prop('disabled', true);

    function validateForm(form){
        let id = form.attr("id");

        switch(id) {
            case "paypalForm":
                if($("#email").val() != "" && $("#psw").val() != ""){
                    return true
                }else{return false}
            case "cardForm":
                if($("#cvv").val() != "" && $("#cardNumber").val() != ""){
                    return true
                }else{return false}
            case "transferForm":
                return true
            default:
              return false;
          }
    }

    function buy() {
            
            $.post("cart_utils.php",
            {
                type: "checkout"
            }
            ,
            function (data, status) {
    
                if (status == "success" && data == "ok") {
                    window.location = "cart.php?success";
                } else {
                    alert("Acquisto non avvenuto");
                    window.location.reload();
                }
            });
    
    }

    function showForm(box) {

        $('#buy').prop('disabled', false);

        $("#paypalForm").css("display", "none");
        $("#cardForm").css("display", "none");
        $("#transferForm").css("display", "none");

        if (box.is(':checked') && box.val() == 'paypal') {
            $("#paypalForm").css("display", "block");
            $("#buy").attr("form","paypalForm");
        } else if (box.is(':checked') && box.val() == 'card') {
            $("#cardForm").css("display", "block");
            $("#buy").attr("form","cardForm");
        } else if (box.is(':checked') && box.val() == 'transfer') {
            $("#transferForm").css("display", "block");
            $("#buy").attr("form","transferForm");
        }
    }

    //Checkout button
    $("#buy").click(function () {

        let selected = 0;
        if($('#paypal').is(':checked')){
            selected = $('#paypalForm');
        }else if($('#card').is(':checked')){
            selected = $('#cardForm');
        }else if($('#transfer').is(':checked')){
            selected = $('#transferForm');
        }
        if(validateForm(selected)){
            buy();
        }else{
            alert("Compila correttamente il form!");
        }
    });

    $("#paypal").click(function () {
        showForm($(this));
    });

    $("#card").click(function () {
        showForm($(this));
    });

    $("#transfer").click(function () {
        showForm($(this));
    });
});



