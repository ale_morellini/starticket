//Displays events
function buildListEvents(eventi) {
    let result = "";

    for (let i = 0; i < eventi.length; i++) {
        let evento = `
                    <div class="col-10 col-sm-8 col-md-6">
                        <a href="selected_event.php?event=${eventi[i]["idevento"]}"> 
                            <div class="card">
                                <img src="${ "images/" + eventi[i]["imgevento"]}" class="card-img-top" alt="${eventi[i]["titoloevento"]}">
                                <div class="card-body">
                                    <h5 class="card-title">${eventi[i]["titoloevento"]}</h5>
                                    <p class="card-text">${eventi[i]["artistaevento"]}</p>
                                </div>
                            </div>
                        </a>
                    </div>
        `;
        result += evento;
    }
    return result;
}

$(document).ready(function () {

    let order = "titoloevento";
    computeEvents();

    //Order by button
    $(".btn").click(function () {
        order = $(this).val();
        computeEvents();
    });

    //Next page button logics
    $("#next").click(function () {
        let count = $(".card").length;
        //If cards are lower than default items per page
        //means that  user is on last page but if not...
        if (count <= $("#itemPerPage").val()) {

            $.post("api_event_category.php",
                {
                    "categoria": $("#categoria").val(),
                }
                ,
                function (data, status) {

                    if (status == "success" && data > $("#pagina").val() * $("#itemPerPage").val()) {
                        let page = $("#pagina").val();
                        let nextpage = parseInt(page) + 1;
                        window.location = "event_list_selector.php?categoria=" + $("#categoria").val() + "&pagina=" + nextpage;
                    } else {
                        alert("Non ci sono ulteriori eventi");
                    }
                });
        }
    });

    //Prev page button logics
    $("#prev").click(function () {
        let page = $("#pagina").val();
        let nextpage = parseInt(page) - 1;

        if (nextpage >= 1) {
            window.location = "event_list_selector.php?categoria=" + $("#categoria").val() + "&pagina=" + nextpage;
        } else {
            //user cannot go below first page
        }
    });

    //AJAX. This function loads in event_list_page
    //a proper view of events of a specific category, in a precise
    //page and correctly ordered
    function computeEvents() {
        $.getJSON("api_list_event.php", {

            "order": order,
            "pagina": $("#pagina").val(),
            "categoria": $("#categoria").val(),
            "itemPerPage": $("#itemPerPage").val()

        }, function (data) {
            let articoli = buildListEvents(data);
            let main = $("#list");
            main.empty();
            main.append(articoli);
        });
    }
});

