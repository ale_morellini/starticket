<?php
require_once 'bootstrap.php';


if(isset($_GET["page"])){
    $templateParams["goTo"] = $_GET["page"];
}

//Base Template
$templateParams["titolo"] = "Starticket";
$templateParams["pagina"] = "footer_page.php";
$templateParams["js"] = array("js/jquery-3.4.1.min.js","js/footer_page.js");
$templateParams["categorie"] = $dbh->getCategories();
if(isUserLoggedIn()){
    $templateParams["notifiche"] = $dbh->getUnreadNotifications($_SESSION["email"]);
}else{
    $templateParams["notifiche"] = "";
}


require 'template/base.php';
?>