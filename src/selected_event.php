<?php
require_once 'bootstrap.php';

$id = $_GET["event"];

$templateParams["titolo"] = "Starticket";
$templateParams["categorie"] = $dbh->getCategories();
$templateParams["evento"] = $dbh->getEventById($id);
$templateParams["pagina"] = "event.php";
if(isUserLoggedIn()){
    $templateParams["notifiche"] = $dbh->getUnreadNotifications($_SESSION["email"]);
}else{
    $templateParams["notifiche"] = "";
}


require 'template/base.php';
?>

