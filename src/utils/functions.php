<?php



function isUserLoggedIn()
{
    return !empty($_SESSION["id"]);
}


function getLoggedType()
{
    return $_SESSION["tipologia"];
}

function registerLoggedUser($user)
{
    $_SESSION["id"] = $user["id"];
    $_SESSION["email"] = $user["email"];
    $_SESSION["nome"] = $user["nome"];
    $_SESSION["cognome"] = $user["cognome"];
    $_SESSION["tipologia"] = $user["tipologia"];
    $_SESSION["indirizzo"] = $user["indirizzo"];
}


function getEmptyEvent()
{
    return array("idevento" => "", "titoloevento" => "", "luogoevento" => "", "cittaevento" => "", "dataevento" => "", "oraevento" => "", "numeroposti" => "", "costoevento" => "", "imgevento" => "", "infoevento" => "", "artistaevento" => "");
}

function getAction($action)
{
    $result = "";
    switch ($action) {
        case 1:
            $result = "Inserisci";
            break;
        case 2:
            $result = "Modifica";
            break;
        case 3:
            $result = "Cancella";
            break;
    }

    return $result;
}


function getNewsletter($val)
{
    $result = "";
    switch ($val) {
        case 1:
            $result = "si";
            break;
        case 0:
            $result = "no";
            break;
    }

    return $result;
}


function getState($state)
{
    $result = "";
    switch ($state) {
        case 1:
            $result = "Attivo";
            break;
        case 0:
            $result = "Non Attivo";
            break;
    }


    return $result;
}

function getOppositeState($state)
{
    $result = "";
    switch ($state) {
        case 1:
            $result = 0;
            break;
        case 0:
            $result = 1;
            break;
    }

    return $result;
}

function getOppositeAction($state)
{
    $result = "";
    switch ($state) {
        case 1:
            $result = "Disattiva";
            break;
        case 0:
            $result = "Attiva";
            break;
    }

    return $result;
}


function uploadImage($path, $image)
{
    $imageName = basename($image["name"]);
    $fullPath = $path . $imageName;

    $maxKB = 500;
    $acceptedExtensions = array("jpg", "jpeg", "png", "gif");
    $result = 0;
    $msg = "";
    //Controllo se immagine è veramente un'immagine
    $imageSize = getimagesize($image["tmp_name"]);
    if ($imageSize === false) {
        $msg .= "File caricato non è un'immagine! ";
    }
    //Controllo dimensione dell'immagine < 500KB
    if ($image["size"] > $maxKB * 1024) {
        $msg .= "File caricato pesa troppo! Dimensione massima è $maxKB KB. ";
    }

    //Controllo estensione del file
    $imageFileType = strtolower(pathinfo($fullPath, PATHINFO_EXTENSION));
    if (!in_array($imageFileType, $acceptedExtensions)) {
        $msg .= "Accettate solo le seguenti estensioni: " . implode(",", $acceptedExtensions);
    }

    //Controllo se esiste file con stesso nome ed eventualmente lo rinomino
    if (file_exists($fullPath)) {
        $i = 1;
        do {
            $i++;
            $imageName = pathinfo(basename($image["name"]), PATHINFO_FILENAME) . "_$i." . $imageFileType;
        } while (file_exists($path . $imageName));
        $fullPath = $path . $imageName;
    }

    //Se non ci sono errori, sposto il file dalla posizione temporanea alla cartella di destinazione
    if (strlen($msg) == 0) {
        if (!move_uploaded_file($image["tmp_name"], $fullPath)) {
            $msg .= "Errore nel caricamento dell'immagine.";
        } else {
            $result = 1;
            $msg = $imageName;
        }
    }
    return array($result, $msg);
}

function dateDiff($date1, $date2)
{

    $date1 = date_create($date1);
    $date2 = date_create($date2);
    $diff = date_diff($date1, $date2);
    echo $diff->format("%a");
}



function isCartEmpty()
{
    if (isset($_SESSION["carrello"])) {
        return false;
    } else {
        return true;
    }
}


function getOrgInfo($idevento){
    $dbh = $GLOBALS["dbh"];
    $email = $dbh->getEventAuthorById($idevento)[0]["credenziali"];
    return $dbh->getOrgByMail($email)[0];
}
