<?php

//Inserts a newsletter notification
function sendNewsNotification($dbh, $eventi, $email)
{
    $titoli = array();
    $utente = $dbh->getClibyMail($email);

    foreach ($eventi as $evento) :
        array_push($titoli, $evento["titoloevento"]);
    endforeach;

    $titoloNews = "Newsletter";
    $descrizione = "Ciao " . $utente[0]["nome"] . "<br>" . "Occhio a questi fantastici eventi disponibili da poco,
        non lasciarteli sfuggire!" . "<br>" . "Gli eventi scelti per te sono:";

    foreach ($titoli as $titolo) :
        $descrizione = $descrizione . " " . $titolo . ",";
    endforeach;

    $descrizione = substr_replace($descrizione, ".", strlen($descrizione) - 1);

    $data = date("Y-m-d");
    $letto = 0;

    $result = $dbh->insertNotification($titoloNews, $descrizione, $data, $letto, $email);
    return $result;
}

function contactClient($dbh, $email)
{

    $utente = $dbh->getCliByMail($email)[0];

    $titolo = "Grazie per il tuo supporto";
    $descrizione = "Congratulazioni " . $utente["nome"] . "!" . "<br>" . "Sei l'utente più affezionato dell'ultimo periodo, ti ringraziamo per il tuo supporto e ci auguriamo possa essere tale anche nel futuro. <br> Lo staff di Starticket.";
    $data = date("Y-m-d");
    $letto = 0;

    $dbh->insertNotification($titolo, $descrizione, $data, $letto, $email);
}


function contactOrganizer($dbh, $email)
{

    $utente = $dbh->getOrgByMail($email)[0];

    $titolo = "Grazie per il tuo supporto";
    $descrizione = "Congratulazioni " . $utente["nome"] . "!" . "<br>" . "Sei l'organizzatore più attivo dell'ultimo periodo, ti ringraziamo per il tuo supporto e per la collaborazione che abbiamo intrapreso. Ci auguriamo possa essere tale anche nel futuro. <br> Lo staff di Starticket.";
    $data = date("Y-m-d");
    $letto = 0;

    $dbh->insertNotification($titolo, $descrizione, $data, $letto, $email);
}

function EventDeleteNotification($dbh, $idevento)
{
    $evento = $dbh->getEventById($idevento);
    if (count($evento) > 0) {
        $evento = $evento[0];
        $email = $dbh->getTicketBought($idevento);
        $titolo = "Evento eliminato";
        $descrizione = "L'evento " . $evento["titoloevento"] . " è stato rimosso dalla piattaforma. Contatta il servizio clienti al 800.000.000 per richiedere il rimborso.";
        $data = date("Y-m-d");
        $letto = 0;

        foreach ($email as $e) {
            $email = $e["credenziali"];
            $result = $dbh->insertNotification($titolo, $descrizione, $data, $letto, $email);
        }
    }
    return $result;
}



function EventModifiedNotification($dbh, $idevento)
{
    $evento = $dbh->getEventById($idevento);
    if (count($evento) > 0) {
        $evento = $evento[0];
        $email = $dbh->getTicketBought($idevento);
        $titolo = "Evento modificato";
        $descrizione = "L'evento " . $evento["titoloevento"] . " è stato modificato come segue. <br>
    data evento: " . $evento["dataevento"] . "
    <br> luogo evento: " . $evento["luogoevento"] . "
    <br> ora evento: " . $evento["oraevento"] . ".";
        $data = date("Y-m-d");
        $letto = 0;

        foreach ($email as $e) {
            $email = $e["credenziali"];
            $result = $dbh->insertNotification($titolo, $descrizione, $data, $letto, $email);
        }
    }
    return $result;
}

function newOrgNotification($dbh)
{
    $email = "amministratore1@starticket.com";
    $titolo = "Nuovo organizzatore";
    $descrizione = "Si è appena iscritto un nuovo organizzatore, puoi decidere di accettarlo nella tua comunity accedendo al portale Organizzatori.";
    $data = date("Y-m-d");
    $letto = 0;

    $result = $dbh->insertNotification($titolo, $descrizione, $data, $letto, $email);
    return $result;
}

function soldOutNotification($dbh, $idevento)
{
    $result = $dbh->getEventAuthorById($idevento)[0];
    $email = $result["credenziali"];
    $titolo = $result["titoloevento"] . " è SOLDOUT";
    $descrizione = "L'evento " . $result["titoloevento"] . " del giorno " . $result["dataevento"] . " alle ore " . $result["oraevento"] . " ha terminato i posti disponibili. I biglietti sono stati molto richiesti, aggiungerne altri se possibile.";
    $data = date("Y-m-d");
    $letto = 0;

    $result = $dbh->insertNotification($titolo, $descrizione, $data, $letto, $email);
    return $result;
}