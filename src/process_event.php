<?php

require_once 'bootstrap.php';


if (!isUserLoggedIn() || !isset($_POST["action"])) {
    header("location: login.php");
}


if ($_POST["action"] == 1) {
    //Inserisco

    list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["imgevento"]);
    if ($result != 0) {

        $titoloevento = $_POST["titoloevento"];
        $artistaevento = $_POST["artistaevento"];
        $luogoevento = $_POST["luogoevento"];
        $cittaevento = $_POST["cittaevento"];
        $dataevento = $_POST["dataevento"];
        $oraevento = $_POST["oraevento"];
        $posti = $_POST["numeroposti"];
        $costoevento = $_POST["costoevento"];
        $infoevento = $_POST["infoevento"];
        $categoria = $_POST["categoria"];
        $autore =  $_SESSION["id"];

        $imgevento = $msg;
        $id = $dbh->insertEvent($titoloevento, $artistaevento, $luogoevento, $cittaevento, $dataevento, $oraevento, $posti, $costoevento, $infoevento, $imgevento, $autore, $categoria);
        if ($id != false) {
            $msg = "Inserimento completato correttamente!";
        } else {
            $msg = "Errore in inserimento!";
        }
    }

    $_SESSION["msg"] = $msg;
    header("location: login.php?action=1");
}

if ($_POST["action"] == 2 && isset($_POST["submit"])) {
    //modifico
    $idevento = $_POST["idevento"];
    $titoloevento = $_POST["titoloevento"];
    $artistaevento = $_POST["artistaevento"];
    $luogoevento = $_POST["luogoevento"];
    $dataevento = $_POST["dataevento"];
    $oraevento = $_POST["oraevento"];
    $posti = $_POST["numeroposti"];
    $costoevento = $_POST["costoevento"];
    $infoevento = $_POST["infoevento"];
    $categoria = $_POST["categoria"];
    $autore = $_SESSION["id"];

    if (isset($_FILES["imgarticolo"]) && strlen($_FILES["imgarticolo"]["name"]) > 0) {
        list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["imgarticolo"]);
        if ($result == 0) {
            header("location: login.php?action=1");
        }
        $imgevento = $msg;
    } else {
        $imgevento = $_POST["oldimg"];
    }

    $dbh->updateEventOfAuthor($idevento, $titoloevento, $artistaevento, $luogoevento, $dataevento, $oraevento, $posti, $costoevento, $infoevento, $imgevento, $autore, $categoria);

    EventModifiedNotification($dbh, $idevento);

    $msg = "Modifica completata correttamente!";
    $_SESSION["msg"] = $msg;
    header("location: login.php?action=1");
}

if (isset($_POST["delete"])) {
    //cancello
    $idevento = $_POST["idevento"];
    $autore = $_SESSION["id"];

    EventDeleteNotification($dbh, $idevento);
    $dbh->deleteTickets($idevento);
    $dbh->deleteEvent($idevento);

    $msg = "Cancellazione completata correttamente!";
    $_SESSION["msg"] = $msg;
    header("location: login.php?action=1");
}
