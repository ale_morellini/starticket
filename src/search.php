<?php
require_once 'bootstrap.php';

//Base Template
$templateParams["titolo"] = "Starticket";

if(isUserLoggedIn()){
    $templateParams["notifiche"] = $dbh->getUnreadNotifications($_SESSION["email"]);
}else{
    $templateParams["notifiche"] = "";
}
$templateParams["pagina"] = "search_list_page.php";
$templateParams["itemPerPage"] = 4;
$templateParams["categorie"] = $dbh->getCategories();

require_once 'template/base.php';
