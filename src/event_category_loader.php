<?php 
require_once 'bootstrap.php';
$templateParams["categorie"] = $dbh->getCategories();

if(!isUserLoggedIn() || !isset($_GET["action"]) || getLoggedType()!= "amministratore" ){
    header("location: login.php");
}

$templateParams["notifiche"] = $dbh->getUnreadNotifications($_SESSION["email"]);

if($_GET["action"] == 1){
    $templateParams["js"] = array("js/jquery-3.4.1.min.js","js/executive_utils.js");
    $templateParams["eventi"] = $dbh->getEvent();
    $templateParams["pagina"] = "event_page.php";
}

if($_GET["action"] == 2){
    $templateParams["js"] = array("js/jquery-3.4.1.min.js","js/executive_utils.js");
    $templateParams["pagina"] = "category_page.php";
}

require 'template/base.php';
?>

