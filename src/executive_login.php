<?php
require_once 'bootstrap.php';

if (!isUserLoggedIn()) {
    header("location: login.php");
}


$templateParams["notifiche"] = $dbh->getUnreadNotifications($_SESSION["email"]);
$templateParams["categorie"] = $dbh->getCategories();
$templateParams["pagina"] = "executive_menu.php";

require 'template/base.php';
 ?>