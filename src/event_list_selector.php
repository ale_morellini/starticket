<?php
require_once 'bootstrap.php';

//Base Template
$templateParams["titolo"] = "Starticket";

if (isset($_GET["categoria"])) {

    $templateParams["categorie"] = $dbh->getCategories(); //All cat. from DB
    $templateParams["categoria"] = $_GET["categoria"];    //Category id from URL
    foreach ($templateParams["categorie"] as $key => $value ):
        if($value["idcategoria"] == $templateParams["categoria"]){
            $templateParams["nomeCategoria"] = $value["nome"];
        }
    endforeach;

    $templateParams["pagina"] = "event_list_page.php";
    $templateParams["itemPerPage"] = 4;
}
if(isUserLoggedIn()){
    $templateParams["notifiche"] = $dbh->getUnreadNotifications($_SESSION["email"]);
}else{
    $templateParams["notifiche"] = "";
}

require_once 'template/base.php';
