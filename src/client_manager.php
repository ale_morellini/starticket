<?php
require_once 'bootstrap.php';

if (!isUserLoggedIn() || !isset($_GET["action"]) || getLoggedType() != "amministratore") {
    header("location: login.php");
}

$templateParams["categorie"] = $dbh->getCategories();
$templateParams["notifiche"] = $dbh->getunreadNotifications($_SESSION["email"]);

if ($_GET["action"] == 1) {
    // clienti attivi
    $val = 1;
    $result = $dbh->getActivatedClient($val);
    $templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/executive_utils.js");
    $templateParams["clienti"] = $result;
    $templateParams["pagina"] = "client_page.php";
}

if ($_GET["action"] == 2) {
    // clienti non attivi
    $val = 0;
    $result = $dbh->getActivatedClient($val);
    $templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/executive_utils.js");
    $templateParams["clienti"] = $result;
    $templateParams["pagina"] = "client_page.php";
}


require 'template/base.php';
