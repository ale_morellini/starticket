<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/event.css">

<?php
$evento = $templateParams["evento"];
if ($evento[0]["numeroposti"] > 0) :
?>
    <div class="d-flex ">
        <div class="col">
        </div>
        <div id="eventView" class="container">
            <div class="flex-row bd-highlight mb-3 flex-direction: row">
                <div class="d-col col-sm-8 col-md-8 col-lg-6 ">
                    <h6><?php echo $evento[0]["titoloevento"]; ?></h6>
                    <img src="<?php echo UPLOAD_DIR . $evento[0]["imgevento"]; ?>" class="img-fluid" alt="<?php echo $evento[0]["titoloevento"]; ?>">
                    <div class="container">
                        <br>
                        <div class="row">
                            <p>Data </p>
                            <p class="eventData"><?php echo $evento[0]["dataevento"]; ?></p>
                        </div>
                        <div class="row">
                            <p>Ora </p>
                            <p class="eventData"><?php echo $evento[0]["oraevento"]; ?></p>
                        </div>
                        <div class="row">
                            <p>Luogo </p>
                            <p class="eventData"><?php echo $evento[0]["luogoevento"]; ?></p>
                        </div>
                        <div class="row">
                            <p>Città </p>
                            <p class="eventData"><?php echo $evento[0]["cittaevento"]; ?></p>
                        </div>
                        <div class="row">
                            <p>Prezzo </p>
                            <p class="eventData"><em class="fas fa-euro-sign"></em><?php echo " ";
                                                                                    echo $evento[0]["costoevento"]; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <h6>Info evento</h6><br>
                    <p id="info"><?php echo $evento[0]["infoevento"]; ?></p>
                </div>
            </div>
        </div>
        <div class="col">
        </div>
    </div>
    <div class="d-flex flex-row justify-content-center">
        <?php if (isUserLoggedIn() && $_SESSION["tipologia"] == "amministratore") : ?>
            <?php if (isset($_GET["action"]) && $_GET["action"] == 1) : ?>
                <a id="goBack" href="event_category_loader.php?action=1" class="btn btn-secondary">Torna al menu</a>
            <?php else : ?>
                <a id="goBack" href="statistics.php" class="btn btn-secondary">Torna al menu</a>
            <?php endif; ?>
        <?php else : ?>
            <?php $today = date("Y-m-d");
            if ($today < $evento[0]["dataevento"]) : ?>
                <button id="addToCart" class="btn btn-primary" type="button" value="<?php echo $evento[0]["idevento"]; ?>"><em class="fas fa-cart-plus"></em> Aggiungi al carrello</button>
            <?php else : ?>
                <button id="scaduto" class="btn btn-primary" type="button" disabled> Scaduto!</button>
            <?php endif; ?>
        <?php endif; ?>
    </div>

<?php else : ?>

    <div class="d-flex ">
        <div class="col">
        </div>
        <div id="eventViewSoldOut" class="container">
            <div class="flex-row bd-highlight mb-3 flex-direction: row">
                <div class="d-col col-sm-8 col-md-8 col-lg-6 ">
                    <h6><?php echo $evento[0]["titoloevento"]; ?></h6>
                    <img id="soldOut" src="<?php echo UPLOAD_DIR . "sold_out.png" ?>" class="img-fluid" alt="evento sold out">
                    <div class="container">
                        <br>
                        <div class="row">
                            <p>Data </p>
                            <p class="eventData"><?php echo $evento[0]["dataevento"]; ?></p>
                        </div>
                        <div class="row">
                            <p>Ora </p>
                            <p class="eventData"><?php echo $evento[0]["oraevento"]; ?></p>
                        </div>
                        <div class="row">
                            <p>Luogo </p>
                            <p class="eventData"><?php echo $evento[0]["luogoevento"]; ?></p>
                        </div>
                        <div class="row">
                            <p>Prezzo </p>
                            <p class="eventData"><em class="fas fa-euro-sign"></em><?php echo " ";echo $evento[0]["costoevento"]; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <h6>Info evento</h6></br>
                    <p id="info"><?php echo $evento[0]["infoevento"]; ?></p>
                </div>
            </div>
        </div>
        <div class="col">
        </div>
    </div>

<?php endif; ?>

<!-- Loading js scripts -->
<?php
$templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/event_page.js");
if (isset($templateParams["js"])) :
    foreach ($templateParams["js"] as $script) :
?>
        <script src="<?php echo $script; ?>"></script>
<?php
    endforeach;
endif;
?>