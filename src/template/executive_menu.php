<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/executive.css">

<div class="container-fluid">
    <div class="row">
        <div id="logout_div" class="col-3">
            <form action="index.php?msg=logout" method="POST">
                <input type="submit" value="logout" class="btn btn-secondary">
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-1"></div>
        <div class="col-md-8 col-sm-10">
            <div id="menu">
                <ul class="list-group">
                    <li class="list-group-item">
                        <a href="event_category_loader.php?action=1">
                            <h5>Eventi</h5>
                        </a>
                        <br>
                        <p>Controlla tutti gli eventi disponibili sulla tua piattaforma ed elimina quelli inappropriati</p> <br>
                    </li>
                    <li class="list-group-item">
                        <a href="event_category_loader.php?action=2">
                            <h5>Categorie</h5>
                        </a>
                        <br>
                        <p>Aggiorna e modifica tutte le categorie presenti nella piattaforma</p><br>
                    </li>
                    <li class="list-group-item">
                        <a href="organizer_manager.php?action=2">
                            <h5>Attivazione Organizzatori</h5>
                        </a>
                        <br>
                        <p>Controlla gli organizzatori appena iscritti che attendono l'attivazione del proprio account</p> <br>
                    </li>
                    <li class="list-group-item">
                        <a href="organizer_manager.php?action=1">
                            <h5>Organizzatori Attivi</h5>
                        </a>
                        <br>
                        <p>Monitora tutti gli organizzatori attivi e i loro eventi</p> <br>
                    </li>
                    <li class="list-group-item">
                        <a href="client_manager.php?action=1">
                            <h5>Clienti attivi</h5>
                        </a>
                        <br>
                        <p>Tieni traccia di tutti i clienti attivi nella tua piattaforma e controlla i loro acquisti</p> <br>
                    </li>
                    <li class="list-group-item">
                        <a href="client_manager.php?action=2">
                            <h5>Clienti Bloccati</h5>
                        </a>
                        <br>
                        <p>Tieni traccia di tutti i clienti bloccati</p> <br>
                    </li>
                    <li class="list-group-item">
                        <a href="statistics.php">
                            <h5>Statistiche</h5>
                        </a>
                        <br>
                        <p>Controlla le statistiche del sito, gli eventi più venduti e gli iscritti più attivi</p> <br>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-2 col-sm-1"></div>
    </div>
</div>