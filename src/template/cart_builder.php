<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/cart.css">

<?php foreach ($templateParams["cartEvents"] as $id => $quantity) :
    $event = $dbh->getEventById($id); ?>
    <div class="container d-flex">
        <div class="col-1">
        </div>
        <div id="eventContainer" class="d-flex row align-items-center justify-content-center">
            <div class="col-4">
                <img src="<?php echo UPLOAD_DIR . $event[0]["imgevento"]; ?>" class="img-fluid" alt="<?php echo $event[0]["titoloevento"]; ?>">
            </div>
            <div class="col">
                <em class="fas fa-euro-sign"></em><em id="cost" title="<?php echo (" ");echo $event[0]["costoevento"]; ?>"><?php echo (" ");echo $event[0]["costoevento"]; ?></em>
            </div>
            <div class="col-4 flex-direction: row justify-content-center align-items-center">
                <div class="col align-items-center">
                    <label for="<?php echo $id; ?>" class="sr-only">Quantità</label>
                    <input id="<?php echo $id; ?>" type="text" class="form-control w-40% " placeholder="<?php echo $quantity ?>" readonly>
                </div>
                <div class="col justify-content-center ">
 
                        <button id="up_<?php echo $id; ?>" type="button" class="btn btn-light up" value="<?php echo $event[0]["numeroposti"]; ?>">+</button>

                        <button id="down_<?php echo $id; ?>" type="button" class="btn btn-light down">-</button>
                </div>
            </div>
            <div class="col">
                <em id="trash_<?php echo $id; ?>" class="fas fa-trash-alt trash" title="<?php echo $id; ?>"></em>
            </div>
        </div>
        <div class="col-1">
        </div>
    </div>
<?php endforeach; ?>

<div class="d-flex flex-row justify-content-center">
    <button id="buy" class="btn btn-primary" type="button" value=""><em class="fas fa-shopping-bag"></em> Acquista</button>
</div>


<!-- Loading js scripts -->
<?php
if (isset($templateParams["js"])) :
    foreach ($templateParams["js"] as $script) :
?>
        <script src="<?php echo $script; ?>"></script>
<?php
    endforeach;
endif;
?>