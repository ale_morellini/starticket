<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/cart_empty.css">

<div class="d-flex justify-content-center">
    <div class="col"></div>
    <div class="col-8">
        <div class="row justify-content-center">
            <h2>Il tuo carrello è vuoto</h2>
        </div>
        <div class="row justify-content-center">
             <i id="alert" class="fas fa-exclamation-circle"></i>
        </div>
        <div class="row"></div>
    </div>
    <div class="col"></div>
</div>

<!-- Loading js scripts -->
<?php
if (isset($templateParams["js"])) :
    foreach ($templateParams["js"] as $script) :
?>
        <script src="<?php echo $script; ?>"></script>
<?php
    endforeach;
endif;
?>