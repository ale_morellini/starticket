<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/login.css">
<div class="row">
    <div class="col-md-2 col-sm-1">
    </div>
    <div class="col-md-8 col-sm-10">
        <form action="#" method="POST">
            <div class="form-group">
                <label for="password">Nuova password</label>
                <input type="password" class="form-control" id="password" name="password">
            </div>
            <div class="form-group">
                <label for="nome">Nome</label>
                <input type="text" class="form-control" id="nome" name="nome" value="<?php echo $templateParams["info"]["nome"] ?>">
            </div>
            <div class="form-group">
                <label for="cognome">Cognome</label>
                <input type="text" class="form-control" id="cognome" name="cognome" value="<?php echo $templateParams["info"]["cognome"] ?>">
            </div>
            <div class="form-group ">
                <label for="indirizzo">Indirizzo</label>
                <input type="text" class="form-control" id="indirizzo" name="indirizzo" value="<?php echo $templateParams["info"]["indirizzo"] ?>">
            </div>


            <div class="form-group ">
                <label for="città">Città</label>
                <input type="text" class="form-control" id="città" name="città" value="<?php echo $templateParams["info"]["citta"] ?>">
            </div>
            <div class="form-group">
                <label for="cap">CAP</label>
                <input type="text" class="form-control" id="cap" name="cap" value="<?php echo $templateParams["info"]["cap"] ?>">
            </div>
            <div class="form-group">
                <label for="stato">Stato</label>
                <input type="text" class="form-control" id="stato" name="stato" value="<?php echo $templateParams["info"]["stato"] ?>">
            </div>
            <div class="form-group">
                <input name="checkbox" id="checkbox" type="checkbox"  <?php
                                                                            if ($templateParams["info"]["newsletter"] == 1) {
                                                                                echo ' checked="checked" ';
                                                                            }

                                                                            ?>>
                <label  for="checkbox">
                    Newsletter
                </label>
            </div>
            <?php if ($_SESSION["tipologia"] == "organizzatore") : ?>
                <div class="form-group">
                    <label for="stato">P.iva</label>
                    <input type="text" class="form-control" id="piva" name="piva" value="<?php echo $templateParams["info"]["piva"] ?>">
                </div>
            <?php endif; ?>
            <div class="d-flex justify-content-between">
                <button id="salva" type="submit" class="btn btn-secondary">Salva modifiche</button>
                <a href="login.php">Annulla</a>
            </div>
        </form>
    </div>
    <div class="col-md-2 col-sm-1">
    </div>
</div>