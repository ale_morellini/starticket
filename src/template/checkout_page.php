<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/checkout.css">


<div class="container-fluid ">
    <div class="d-row d-flex justify-content-center ">
        <h3>Riepilogo</h3>
    </div>
    <div class="d-row d-flex ">
        <div class="col">
        </div>
        <div id="info" class="col-10 col-sm-8 col-md-6 ">
            <div id="infoTitle" class="d-row ">
                <h3>Dati Fatturazione</h3>
            </div>
            <label for="nome" class="sr-only">Nome</label>
            <input id="nome" class="form-control" type="text" placeholder="<?php echo $_SESSION["nome"] ?>" readonly><br>
            <label for="cognome" class="sr-only">Cognome</label>
            <input id="cognome" class="form-control" type="text" placeholder="<?php echo $_SESSION["cognome"] ?> " readonly><br>
            <label for="indirizzo" class="sr-only">Indirizzo</label>
            <input id="indirizzo" class="form-control" type="text" placeholder="<?php echo $_SESSION["indirizzo"] ?>" readonly><br>
        </div>
        <div class="col">
        </div>
    </div>

    <div class="d-row d-flex ">
        <div class="col">
        </div>
        <div id="choiseContainer" class="col-10 col-sm-8 col-md-6 ">
            <div id="title" class="d-row ">
                <h3>Paga con</h3>
            </div>
            <div class="d-row d-flex option">
                <input id="paypal" type="radio" name="choise" value="paypal"><br>
                <em class="fab fa-paypal choiseLogo"></em>
                <label for="paypal">PayPal</label>
            </div>
            <div class="row d-flex">
                <form id="paypalForm">
                    <div class="form-check">
                        <label for="email"> Email</label>
                        <input type="email" class="form-control" id="email" aria-describedby="emailHelp" required>
                        <small id="emailHelp" class="form-text text-muted">Inserisci la tua mail di PayPal</small>
                    </div>
                    <div class="form-check">
                        <label for="psw"> Password</label>
                        <input type="password" class="form-control" id="psw" required>
                    </div>
                </form>
            </div>
            <div class="d-row d-flex option">
                <input id="card" type="radio" name="choise" value="card"><br>
                <em class="fas fa-credit-card choiseLogo"></em>
                <label for="card"> Carta di credito</label>
            </div>
            <div class="d-row d-flex">
                <form id="cardForm">
                    <div class="form-group">
                        <label for="cardNumber"> Numero carta</label>
                        <input type="email" class="form-control" id="cardNumber" required>
                    </div>
                    <div class="form-group">
                        <label for="cvv"> CVV</label>
                        <input type="password" class="form-control" id="cvv" required>
                    </div>
                </form>

            </div>
            <div class="d-row d-flex option">
                <input id="transfer" type="radio" name="choise" value="transfer"><br>
                <em class="fas fa-money-bill-alt choiseLogo"></em>
                <label for="transfer"> Bonifico</label>
            </div>
            <div class="d-row d-flex">
                <form class="form-check" id="transferForm">
                    <p>Effettuare un bonifico sul conto</p>
                    <input type="text" readonly class="form-control-plaintext" id="staticIban" value="IT60 X054 2811 1010 0000 0123 456">
                    <label for="staticIban" class="sr-only">Iban</label>
                </form>
            </div>

        </div>
        <div class="col">
        </div>
    </div>
</div>


<div class="d-flex flex-row justify-content-center">
    <!-- form attr is not set but is set on load -->
    <button id="buy" class="btn btn-primary" type="submit" form=""><em class="fas fa-shopping-bag"></em> Acquista</button>
</div>

<!-- Loading js scripts -->
<?php
if (isset($templateParams["js"])) :
    foreach ($templateParams["js"] as $script) :
?>
        <script src="<?php echo $script; ?>"></script>
<?php
    endforeach;
endif;
?>