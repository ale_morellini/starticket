<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/executive_page.css">

<div class="container-fluid">
    <div class="row">
        <div id="logout_div" class="col-3">
            <a href="executive_login.php" class="btn btn-secondary">Torna al menu</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-1 "></div>
        <div class="col-md-8 col-sm-10 table-responsive-md">
            <?php if (count($templateParams["eventi"]) == 0) : ?>
                <div>
                    <h2>Non sono presenti Eventi</h2>
                </div>
            <?php else : ?>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col" id="num"></th>
                            <th scope="col" id="titolo" headers="num">Titolo</th>
                            <th scope="col" id="info" headers="num"></th>
                            <th scope="col" id="org" headers="num">Organizatore</th>
                            <th scope="col" id="cestino" headers="num"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 0; ?>
                        <?php foreach ($templateParams["eventi"] as $evento) : ?>
                            <?php $i += 1; ?>

                            <tr>
                                <th scope="row" id="num-<?php echo $i ?>" headers="num"><?php echo $i ?></th>
                                <td headers="titolo num-<?php echo $i ?>"><?php echo $evento["titoloevento"] ?></td>
                                <td headers="info num-<?php echo $i ?>"><a href="selected_event.php?event=<?php echo $evento["idevento"] ?>&action=1">Maggori info</a></td>
                                <td headers="org num-<?php echo $i ?>">Organizzatore: <?php echo getOrgInfo($evento["idevento"])["nome"];
                                                    echo " ";
                                                    echo getOrgInfo($evento["idevento"])["cognome"];
                                                    echo "<br>";
                                                    echo getOrgInfo($evento["idevento"])["email"]; ?></td>
                                <td headers="cestino num-<?php echo $i ?>"><em id="<?php echo $evento["idevento"]; ?>" class="fas fa-trash-alt trash"></em></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
        <div class="col-md-2 col-sm-1"></div>
    </div>
</div>


<!-- Loading js scripts -->
<?php
if (isset($templateParams["js"])) :
    foreach ($templateParams["js"] as $script) :
?>
        <script src="<?php echo $script; ?>"></script>
<?php
    endforeach;
endif;
?>