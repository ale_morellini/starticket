<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/login.css">

<?php
require_once 'bootstrap.php';
if ($_SESSION["tipologia"] == "organizzatore") {
    $info = $dbh->getOrgbyMail($_SESSION["email"])[0];
    $prossimiEventi = $dbh->getNextEvent($info["id"]);
    $vecchiEventi = $dbh->getOldEvent($info["id"]);
}
if ($_SESSION["tipologia"] == "cliente") {
    $info = $dbh->getClibyMail($_SESSION["email"])[0];
    $prossimiEventi = $dbh->getNextTicket($info["id"]);
    $vecchiEventi = $dbh->getOldTicket($info["id"]);
}
?>


<div class="container-fluid">
    <div class="row">
        <div class="col-9">
            <a href="login.php" id="datiButton" class="btn btn-secondary">I tuoi dati</a>
            <a href="login.php?action=1" id="eventiButton" class="btn btn-secondary">I tuoi eventi</a>
        </div>
        <div class="col-3 d-flex justify-content-end">
            <form action="index.php?msg=logout" method="POST">
                <input type="submit" value="logout" class="btn btn-secondary">
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2 col-sm-1"></div>
        <div class="col-sm-10 col-md-8">
            <?php if (count($prossimiEventi) > 0) : ?>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" id="eventi">Eventi futuri</th>
                            <th scope="col" id="img"></th>
                            <?php if ($_SESSION["tipologia"] == "cliente") : ?>
                                <th scope="col" id="data">Data acquisto</th>
                            <?php endif; ?>
                            <th scope="col" id="azione"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($prossimiEventi as $evento) : ?>
                            <tr>
                                <td headers="eventi"><?php echo $evento["titoloevento"] ?> <br> Data: <?php echo $evento["dataevento"] ?><br> Ora: <?php echo $evento["oraevento"] ?></td>
                                <td headers="img"> <img src="images/<?php echo $evento["imgevento"] ?>" alt="immagine evento" class="eventimg"> </td>
                                <?php if ($_SESSION["tipologia"] == "cliente") : ?>
                                    <td headers="data"><?php echo $evento["databiglietto"] ?></td>
                                    <td headers="azione"><a href="selected_event.php?event=<?php echo $evento["id"] ?>">Maggiori info</a></td>
                                <?php else : ?>
                                    <td headers="azione"><a href="event_manager.php?action=2&id=<?php echo $evento["id"] ?>">Modifica</a></td>

                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
            <?php if (count($vecchiEventi) > 0) : ?>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th>Eventi pasati</th>
                            <th></th>
                            <?php if ($_SESSION["tipologia"] == "cliente") : ?>
                                <th>Data acquisto</th>
                            <?php endif; ?>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($vecchiEventi as $evento) : ?>
                            <tr>
                                <td><?php echo $evento["titoloevento"] ?> <br> Data: <?php echo $evento["dataevento"] ?><br> Ora: <?php echo $evento["oraevento"] ?></td>
                                <td> <img src="images/<?php echo $evento["imgevento"] ?>" alt="immagine evento" class="eventimg"> </td>
                                <?php if ($_SESSION["tipologia"] == "cliente") : ?>
                                    <td><?php echo $evento["databiglietto"] ?></td>
                                    <td><a href="selected_event.php?event=<?php echo $evento["id"] ?>">Maggiori info</a></td>
                                <?php else : ?>
                                    <td><a href="event_manager.php?action=2&id=<?php echo $evento["id"] ?>">Modifica</a></td>

                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
            <?php if (count($prossimiEventi) == 0 && count($vecchiEventi) == 0) : ?>
                <h3>Non sono presenti eventi</h3>
            <?php endif; ?>
        </div>
        <div class="col-md-2 col-sm-1"></div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-1"></div>
        <div class="col-sm-10 col-md-8">
            <?php if ($_SESSION["tipologia"] == "organizzatore") : ?>
                <a href="event_manager.php?action=1"><button id="aggiungi" type="button" class="btn btn-secondary"><i class="fas fa-plus"></i> Aggiungi evento</button></a>
            <?php endif; ?>
        </div>
        <div class="col-md-2 col-sm-1"></div>
    </div>
</div>