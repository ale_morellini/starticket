<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/event_list_page.css">

<!-- Setting up the page such as -->
<?php

$categoria = $templateParams["categoria"];

?>

<h4><?php echo $templateParams["nomeCategoria"] ?></h4>
<div id="choiseContainer" class="container-fluid d-flex ">
    <div class="d-row d-flex align-items-center">
        <div id="choise" class="btn-group" role="group" aria-label="Scelta ordinamento">
            <button type="button" class="btn btn-secondary" value="dataevento">Data</button>
            <button type="button" class="btn btn-secondary" value="titoloevento">Titolo</button>
            <button type="button" class="btn btn-secondary" value="luogoevento">Luogo</button>
            <input id="categoria" type="hidden" value="<?php echo $categoria;?>"/>
            <input id="pagina" type="hidden" value="<?php echo $_GET["pagina"];?>"/>
            <input id="itemPerPage" type="hidden" value="<?php echo $templateParams["itemPerPage"];?>"/>
        </div>
    </div>
</div>

<div id="resultList" class="container-fluid">
    <div id="list" class="row d-flex flex-direction: row justify-content-around">
        <section>

        </section>
    </div>
</div>

<nav id="pageButtons" aria-label="navigation buttons">
    <ul class="pagination justify-content-center">
        <li class="page-item">
            <button id="prev" class="btn">Indietro</button>
        </li>
        <li class="page-item">
            <button id="next" class="btn"> Avanti </button>
        </li>
    </ul>
</nav>

<!-- Loading js scripts -->
<?php
        $templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/list_page.js");
        if (isset($templateParams["js"])) :
            foreach ($templateParams["js"] as $script) :
        ?>
        <script src="<?php echo $script; ?>"></script>
<?php
            endforeach;
        endif;
?>