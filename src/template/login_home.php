<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/login.css">

<?php
require_once 'bootstrap.php';
if ($_SESSION["tipologia"] == "organizzatore") {
  $info = $dbh->getOrgbyMail($_SESSION["email"])[0];
  $info["tipologia"] = "Account business";
}
if ($_SESSION["tipologia"] == "cliente") {
  $info = $dbh->getClibyMail($_SESSION["email"])[0];
  $info["tipologia"] = "Account personale";
}
?>
<div class="container-fluid">
  <div class="row">
    <div class="col-9" >
      <a href="login.php" id="dati" class="btn btn-secondary">I tuoi dati</a>
      <a href="login.php?action=1" id="eventiButton" class="btn btn-secondary">I tuoi eventi</a>
    </div>
    <div class="col-3 d-flex justify-content-end">
      <form action="index.php?msg=logout" method="POST">
        <input id="logout" type="submit" value="logout" class="btn btn-secondary">
      </form>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-1 col-md-2"></div>
    <div class="col-md-8 col-sm-10 mainCol">
      <ul class="list-group">
        <li class="list-group-item">Tipologia account: <br> <?php echo $info["tipologia"] ?> </li>
        <li class="list-group-item">Nome: <br> <?php echo $info["nome"] ?></li>
        <li class="list-group-item">Cognome: <br> <?php echo $info["cognome"] ?> </li>
        <li class="list-group-item">Indirizzo: <br><?php echo $info["indirizzo"] ?>
          <br> <?php echo $info["citta"] ?> <br> <?php echo  $info["cap"] ?></li>
        <li class="list-group-item">Email: <br> <?php echo  $info["email"] ?></li>
        <li class="list-group-item">iscrizione alla newsletter: <?php echo getNewsletter($info["newsletter"]) ?></li>
        <?php if ($_SESSION["tipologia"] == "organizzatore") : ?>
          <li class="list-group-item">P.Iva: <br> <?php echo  $info["piva"] ?></li>
        <?php endif; ?>
      </ul>
    </div>
    <div class="col-sm-1 col-md-2"></div>
  </div>

  <div class="row">
    <div class="col-sm-1 col-md-2"></div>
    <div class="col-sm-10 col-md-8">
      <a href="login_manager.php" id="modifica" class="btn btn-secondary"><em class="far fa-edit"></em> Modifica Dati</a>
    </div>
    <div class="col-sm-1 col-md-2"></div>
  </div>
</div>