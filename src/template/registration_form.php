<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/login.css">
<div class="container-fluid">
    <div class="row">
        <?php if (isset($templateParams["formmsg"])) : ?>
            <p><?php echo $templateParams["formmsg"]; ?></p>
        <?php endif; ?>
    </div>
    <div class="row">
        <div class="col">
        </div>
        <div class="col-sm-10 col-md-10 col-lg-10 col-xl-10">
            <form action="#" method="POST">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" required>
                    </div>
                </div>


                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="nome">Nome</label>
                        <input type="text" class="form-control" id="nome" name="nome" required>
                    </div>
                    <div class="dorm-group col-md-6">
                        <label for="cognome">Cognome</label>
                        <input type="text" class="form-control" id="cognome" name="cognome" required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="indirizzo">Indirizzo</label>
                    <input type="text" class="form-control" id="indirizzo" name="indirizzo" required>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="città">Città</label>
                        <input type="text" class="form-control" id="città" name="città" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="cap">CAP</label>
                        <input type="text" class="form-control" id="cap" name="cap" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="stato">Stato</label>
                        <input type="text" class="form-control" id="stato" name="stato" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="gridCheck" name="checkbox">
                            <label class="form-check-label" for="gridCheck">
                                Sono un organizzatore
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="piva">Partita IVA</label>
                        <input type="text" class="form-control" id="piva" name="piva">
                    </div>
                    <div class="form-group col-md-4">
                        <input name="news_checkbox" id="news_checkbox" type="checkbox">
                        <label for="news_checkbox">
                            Newsletter
                        </label>
                    </div>
                    <div class="form-group col-md-4">
                        <input name="privacy_checkbox" id="privacy_checkbox" type="checkbox" required>
                        <label for="privacy_checkbox">
                            Accetto l'informativa sulla privacy
                        </label>
                    </div>
                </div>

                <br>
                <button id="registrati" type="submit" class="btn btn-primary">Registrati</button>
            </form>
        </div>
        <div class="col">
        </div>
    </div>
</div>