<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/footer_page.css">


<?php
    //just checking which page has to be loaded
    if(isset($templateParams["goTo"])) :
        $title = $templateParams["goTo"];
?>
 <input id="goToPage" type="hidden" value="<?php echo $title;?>"/>
 <?php
    endif;
?>

<div id="textContainer" class="container-fluid d-flex">
        <div class="col">
        </div>
        <div class="col-sm-10 col-md-8 justify-content-center">
            <section>

            </section>
        </div>
        <div class="col">
        </div>
</div>

<!-- Loading js scripts -->
<?php
if (isset($templateParams["js"])) :
    foreach ($templateParams["js"] as $script) :
?>
        <script src="<?php echo $script; ?>"></script>
<?php
    endforeach;
endif;
?>