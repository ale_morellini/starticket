<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/statistics.css">

<div class="container-fluid">
    <div class="row">
        <div id="logout_div" class="col-3">
            <a href="executive_login.php" class="btn btn-secondary">Torna al menu</a>
        </div>
    </div>
    <div class="row ad">
        <div class="col-md-2 col-sm-1 "></div>
        <div class="col-md-8 col-sm-10 table-responsive-md">
            <?php if (count($templateParams["eventi"]) == 0) : ?>
                <div>
                    <h3>Non sono presenti eventi inseriti recentemente</h3>
                </div>
            <?php else : ?>
                <h3>Eventi inseriti recentemente</h3>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col" id="num"></th>
                            <th scope="col" id="titolo" headers="num">Titolo</th>
                            <th scope="col" id="img" headers="num"></th>
                            <th scope="col" id="data" headers="num">data inserimento</th>
                            <th scope="col" id="org" headers="num">Organizatore</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 0; ?>
                        <?php foreach ($templateParams["eventi"] as $evento) : ?>
                            <?php $i += 1; ?>
                            <tr>
                                <th scope="row" id="num-<?php echo $i ?>" headers="num"><?php echo $i ?></th>
                                <td headers="titolo num-<?php echo $i ?>"><?php echo $evento["titoloevento"] ?></td>
                                <td headers="img num-<?php echo $i ?>"><img src="images/<?php echo $evento["imgevento"] ?>" alt="immagine evento" class="eventimg" style="width: 60px"></td>
                                <td headers="data num-<?php echo $i ?>"><?php echo $evento["datainserimento"] ?></td>
                                <td headers="org num-<?php echo $i ?>">Organizzatore: <?php echo getOrgInfo($evento["idevento"])["nome"];
                                                    echo " ";
                                                    echo getOrgInfo($evento["idevento"])["cognome"];
                                                    echo "<br>";
                                                    echo getOrgInfo($evento["idevento"])["email"]; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
        <div class="col-md-2 col-sm-1"></div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-1"></div>
        <div class="col-md-8 col-sm-10 part">
            <button class="btn btn-secondary" id="sendNewsletter">Invia newsletter</button>
        </div>
        <div class="col-md-2 col-sm-1"></div>
    </div>

    <div class="row ad">
        <div class="col-md-2 col-sm-1"></div>
        <div class="col-md-8 col-sm-10 part">
            <h3>Evento più venduto</h3>
            <a href="selected_event.php?event=<?php echo $templateParams["+_venduto"]["idevento"] ?>&action=2">
                <div class="card">
                    <img src="images/<?php echo $templateParams["+_venduto"]["imgevento"] ?>" class="card-img-top" alt="<?php echo $templateParams["+_venduto"]["titoloevento"] ?>">
                    <div class="card-body">
                        <h4 class="card-title"><?php echo $templateParams["+_venduto"]["titoloevento"] ?></h4>
                        <p class="card-text"><?php echo $templateParams["+_venduto"]["artistaevento"] ?></p>
                    </div>
                    <p>Biglietti venduti per un totale di <?php echo $templateParams["+_venduto"]["count"] ?> unità</p>
                </div>
            </a>
        </div>
        <div class="col-md-2 col-sm-1"></div>
    </div>

    <div class="row ad">
        <div class="col-md-2 col-sm-1"></div>
        <div class="col-md-8 col-sm-10 ">
            <h3>Organizzatore più attivo</h3>
            <ul class="list-group">
                <li class="list-group-item">Nome: <br> <?php echo $templateParams["org_+_attivo"]["nome"] ?></li>
                <li class="list-group-item">Cognome: <br> <?php echo $templateParams["org_+_attivo"]["cognome"] ?> </li>
                <li class="list-group-item">Indirizzo: <br><?php echo $templateParams["org_+_attivo"]["indirizzo"] ?>
                    <br> <?php echo $templateParams["org_+_attivo"]["citta"] ?> <br> <?php echo  $templateParams["org_+_attivo"]["cap"] ?></li>
                <li class="list-group-item">Email: <br> <?php echo  $templateParams["org_+_attivo"]["email"] ?></li>
                <li class="list-group-item">P.Iva: <br> <?php echo  $templateParams["org_+_attivo"]["piva"] ?></li>
                <li class="list-group-item"><?php echo  $templateParams["num_eventi"] ?> eventi inseriti sulla piattaforma</li>
            </ul>
        </div>
        <div class="col-md-2 col-sm-1"></div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-1"></div>
        <div class="col-md-8 col-sm-10 part">
            <button class="btn btn-secondary" id="contactOrg">Contatta organizzatore</button>
            <input id="orgEmail" type="hidden" value="<?php echo  $templateParams["org_+_attivo"]["email"] ?>">

        </div>
        <div class="col-md-2 col-sm-1"></div>
    </div>


    <div class="row ad">
        <div class="col-md-2 col-sm-1"></div>
        <div class="col-md-8 col-sm-10">
            <h3>Cliente più attivo</h3>
            <ul class="list-group">
                <li class="list-group-item">Nome: <br> <?php echo $templateParams["cli_+_attivo"]["nome"] ?></li>
                <li class="list-group-item">Cognome: <br> <?php echo $templateParams["cli_+_attivo"]["cognome"] ?> </li>
                <li class="list-group-item">Indirizzo: <br><?php echo $templateParams["cli_+_attivo"]["indirizzo"] ?>
                    <br> <?php echo $templateParams["cli_+_attivo"]["citta"] ?> <br> <?php echo  $templateParams["cli_+_attivo"]["cap"] ?></li>
                <li class="list-group-item">Email: <br> <?php echo  $templateParams["cli_+_attivo"]["email"] ?></li>
                <li class="list-group-item"><?php echo  $templateParams["num_biglietti"] ?> biglietti acquistati su questa piattaforma</li>
            </ul>
        </div>
        <div class="col-md-2 col-sm-1"></div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-1"></div>
        <div class="col-md-8 col-sm-10 part">
            <button class="btn btn-secondary" id="contactCli">Contatta cliente</button>
            <input id="cliEmail" type="hidden" value="<?php echo  $templateParams["cli_+_attivo"]["email"] ?>">
        </div>
        <div class="col-md-2 col-sm-1"></div>
    </div>

</div>



<!-- Loading js scripts -->
<?php
if (isset($templateParams["js"])) :
    foreach ($templateParams["js"] as $script) :
?>
        <script src="<?php echo $script; ?>"></script>
<?php
    endforeach;
endif;
?>