<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/checkout_success.css">

<div class="d-flex justify-content-center">
    <div class="col"></div>
    <div class="col-8">
        <div class="row justify-content-center">
            <h2>Acquisto completato con successo!</h2>
        </div>
        <div class="row justify-content-center">
             <i id="alert" class="far fa-check-circle"></i>
        </div>
        <div class="row"></div>
    </div>
    <div class="col"></div>
</div>

<!-- Loading js scripts -->
<?php
if (isset($templateParams["js"])) :
    foreach ($templateParams["js"] as $script) :
?>
        <script src="<?php echo $script; ?>"></script>
<?php
    endforeach;
endif;
?>