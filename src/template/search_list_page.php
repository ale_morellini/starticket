<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/event_list_page.css">

<div class="container-fluid">
    <input id="ricerca" type="hidden" value="<?php echo $_GET["substr"]; ?>">
    <input id="pagina" type="hidden" value="<?php echo $_GET["pagina"]; ?>">
    <input id="itemPerPage" type="hidden" value="<?php echo $templateParams["itemPerPage"]; ?>">
</div>

<div class="container-fluid">
    <div id="list" class="row d-flex flex-direction: row justify-content-around">
        
    </div>
</div>

<nav id="pageButtons" aria-label="navigation buttons">
    <ul class="pagination justify-content-center">
        <li class="page-item">
            <button id="prev" class="btn">
                Indietro</button> </li> <li class="page-item">
                    <button id="next" class="btn">Avanti</button>
        </li>
    </ul>
</nav>

<!-- Loading js scripts -->
<?php
$templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/search.js");
if (isset($templateParams["js"])) :
    foreach ($templateParams["js"] as $script) :
?>
        <script src="<?php echo $script; ?>"></script>
<?php
    endforeach;
endif;
?>