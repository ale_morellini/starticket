<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/executive_page.css">

<div class="container-fluid">
    <div class="row">
        <div id="logout_div" class="col-3">
            <a href="executive_login.php" class="btn btn-secondary">Torna al menu</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-1"></div>
        <div class="col-md-8 col-sm-10 d-flex flex-column">
            <div class="table-responsive-md">
                <?php if (count($templateParams["categorie"]) == 0) : ?>
                    <div>
                        <h3>Non sono presenti categorie</h3>
                    </div>
                <?php else : ?>
                    <h3>Categorie</h3>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col" id="num"></th>
                                <th scope="col" id="nome" headers="num">Nome categoria</th>
                                <th scope="col" id="cestino" headers="num"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0; ?>
                            <?php foreach ($templateParams["categorie"] as $categoria) : ?>
                                <?php $i += 1; ?>
                                <tr>
                                    <th scope="row" id="num-<?php echo $i ?>" headers="num"><?php echo $i ?></th>
                                    <td headers="num-<?php echo $i ?> nome"><?php echo $categoria["nome"] ?></td>
                                    <td headers="cestino num-<?php echo $i ?>"><em id="<?php echo $categoria["idcategoria"]; ?>" class="fas fa-trash-alt trashCat"></em></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </div>
            <div id="newCat">
                <h4>Inserimento nuova categoria</h4>
                <div class="form-group">
                    <label for="inputCategoria" class="sr-only">Inserimento nuova categoria </label>
                    <input type="text" class="form-control" id="inputCategoria" name="categoria">
                    <button id="btnCategoria" type="submit" class="btn btn-secondary">Inserisci</button>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-sm-1"></div>
    </div>
</div>

<!-- Loading js scripts -->
<?php
if (isset($templateParams["js"])) :
    foreach ($templateParams["js"] as $script) :
?>
        <script src="<?php echo $script; ?>"></script>
<?php
    endforeach;
endif;
?>