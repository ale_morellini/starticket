<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/login.css">

<div class="container-fluid">
    <div class="row">
        <?php if (isset($templateParams["formmsg"])) : ?>
            <p><?php echo $templateParams["formmsg"]; ?></p>
        <?php endif; ?>
    </div>
    <div class="row">
        <div class="col">
        </div>
        <div class="col-sm-10 col-md-6 col-lg-6 col-xl-6">
            <div>
                <form action="<?php if(isset($_GET["cartLogin"])){echo "login.php?cartLogin=1";}else{echo "login.php";}?>" method="POST">
                    <h2>Accedi a starticket</h2>
                    <?php if (isset($templateParams["errorelogin"])) : ?>
                        <p><?php echo $templateParams["errorelogin"]; ?></p>
                    <?php endif; ?>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Accedi</button>
                </form>
            </div>
            <br> <br>
            <a href="registration.php">
                <p>Non sei ancora iscritto? Registrati qui</p>
            </a>

        </div>
        <div class="col">
        </div>
    </div>
</div>