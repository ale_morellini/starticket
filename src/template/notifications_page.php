<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/notification.css">

<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 col-sm-1"></div>
        <div class="col-md-8 col-sm-10">
            <?php if (count($templateParams["notifiche_lette"]) == 0) : ?>
                <div>
                    <h3>Non sono presenti notifiche</h3>
                </div>
            <?php else : ?>
                <h3>Notifiche</h3>
                <div class="accordion" id="accordion">
                    <?php foreach ($templateParams["notifiche_lette"] as $notifica) : ?>
                        <div id="card-<?php echo $notifica["idnotifica"] ?>" class="card">

                            <div class="card-header" id="heading_<?php echo $notifica["idnotifica"] ?>">
                                <div id="div_<?php echo $notifica["idnotifica"] ?>">
                                    <button class="btn btn-link link" type="button" data-toggle="collapse" data-target="#not_<?php echo $notifica["idnotifica"] ?>" aria-expanded="false" aria-controls="not_<?php echo $notifica["idnotifica"] ?>" value="<?php echo $notifica["idnotifica"] ?>">
                                        <?php echo $notifica["titolo"] ?>
                                    </button>
                                    <?php if ($notifica["letto"] == 0) : ?>
                                        <em id="close_<?php echo $notifica["idnotifica"] ?>" class="far fa-envelope"></em>
                                    <?php else : ?>
                                        <em id="open_<?php echo $notifica["idnotifica"] ?>" class="far fa-envelope-open"></em>
                                    <?php endif; ?>
                                </div>
                                <div class="d-flex w-100 justify-content-between">
                                    <small> <?php echo dateDiff($notifica["datanotifica"], date("Y-m-d")) ?> giorni fa</small>
                                    <em id="<?php echo $notifica["idnotifica"] ?>" class="fas fa-trash-alt trash"></em>
                                </div>
                            </div>
                            <div id="not_<?php echo $notifica["idnotifica"] ?>" class="collapse" aria-labelledby="heading_<?php echo $notifica["idnotifica"] ?>" data-parent="#accordion">
                                <div class="card-body">
                                    <?php echo $notifica["descrizione"] ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-md-2 col-sm-1"></div>
    </div>
</div>

<!-- Loading js scripts -->
<?php
if (isset($templateParams["js"])) :
    foreach ($templateParams["js"] as $script) :
?>
        <script src="<?php echo $script; ?>"></script>
<?php
    endforeach;
endif;
?>