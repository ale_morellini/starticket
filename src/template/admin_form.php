<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/login.css">

<?php

$evento = $templateParams["evento"];
$azione = getAction($templateParams["azione"])
?>
<div class="container-fluid  justify-content-center">
    <div class="row">
        <div class="col">
        </div>
        <div class="col-md-6">
            <form action="process_event.php" method="POST">
                <h2>Gestisci Articolo</h2>
                <?php if ($evento == null) : ?>
                    <p>Evento non trovato</p>
                <?php else : ?>

                    <div class="form-group">
                        <label for="titoloevento">Titolo:</label>
                        <input type="text" id="titoloevento" name="titoloevento" class="form-control" value="<?php echo $evento["titoloevento"]; ?>">
                    </div>
                    <div class="form-group">
                        <label for="artistaevento">Artista</label>
                        <input type="text" id="artistaevento" name="artistaevento" class="form-control" value="<?php echo $evento["artistaevento"]; ?>">
                    </div>
                    <div class="form-group">
                        <label for="luogoevento">Luogo</label>
                        <input type="text" id="luogoevento" name="luogoevento" class="form-control" value="<?php echo $evento["luogoevento"]; ?>">
                    </div>
                    <div class="form-group">
                        <label for="cittaevento">Città</label>
                        <input type="text" id="cittaevento" name="cittaevento" class="form-control" value="<?php echo $evento["cittaevento"]; ?>">
                    </div>
                    <div class="form-group">
                        <label for="dataevento">Data</label>
                        <input type="date" id="dataevento" name="dataevento" class="form-control" value="<?php echo $evento["dataevento"]; ?>">
                    </div>

                    <div class="form-group">
                        <label for="oraevento">Ora</label>
                        <input type="time" id="oraevento" name="oraevento" class="form-control" value="<?php echo $evento["oraevento"]; ?>">
                    </div>
                    <div class="form-group">
                        <label for="numeroposti">NumeroPosti</label>
                        <input type="number" id="numeroposti" name="numeroposti" class="form-control" value="<?php echo $evento["numeroposti"]; ?>">
                    </div>
                    <div class="form-group">
                        <label for="costoevento">Costo</label>
                        <input type="number" id="costoevento" name="costoevento" class="form-control" value="<?php echo $evento["costoevento"]; ?>">
                    </div>
                    <div class="form-group">
                        <label for="infoevento">Info Evento</label>
                        <textarea class="form-control" id="infoevento" name="infoevento" rows="3"><?php echo $evento["infoevento"]; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="imgevento">Immagine</label>
                        <input type="file" class="form-control-file" id="imgevento" name="imgevento">
                        <?php if ($templateParams["azione"] != 1) : ?>
                            <img class="eventimg" src="<?php echo UPLOAD_DIR . $evento["imgevento"]; ?>" alt="" />
                        <?php endif; ?>
                    </div>
                    <fieldset>
                        <legend>Categorie</legend>
                        <div class="radio">
                            <?php foreach ($templateParams["categorie"] as $categoria) : ?>

                                <input id="<?php echo $categoria["nome"]; ?>" type="radio" name="categoria" value="<?php echo $categoria["idcategoria"]; ?>" <?php if ($templateParams["azione"] != 1) {
                                                                                                                                                                    if ($categoria["idcategoria"] == $evento["categoria"]) {
                                                                                                                                                                        echo ' checked="checked" ';
                                                                                                                                                                    }
                                                                                                                                                                } ?>>
                                <label for="<?php echo $categoria["nome"]; ?>"> <?php echo $categoria["nome"]; ?></label>
                                <br>

                            <?php endforeach; ?>
                        </div>

                    </fieldset>
                    <div class="d-flex justify-content-between">
                        <input id="aggiungi" type="submit" name="submit" value="<?php echo $azione; ?> Evento" class="btn btn-secondary" />
                        <?php if ($templateParams["azione"] != 1) : ?>
                            <input id="elimina" type="submit" name="delete" value="Elimina Evento" class="btn btn-secondary" />
                            <input type="hidden" name="idevento" value="<?php echo $evento["idevento"]; ?>" />
                            <input type="hidden" name="oldimg" value="<?php echo $evento["imgevento"]; ?>" />
                        <?php endif; ?>
                        <a href="login.php?action=1">Annulla</a>
                        <input type="hidden" name="action" value="<?php echo $templateParams["azione"]; ?>" />
                    </div>
                <?php endif; ?>
            </form>
        </div>
        <div class="col">
        </div>
    </div>
</div>