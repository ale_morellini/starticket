<!doctype html>
<html lang="it">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Css della pagina di base -->
    <link rel="stylesheet" type="text/css" href="css/base.css">
    <script src="https://use.fontawesome.com/releases/v5.11.2/js/all.js" data-auto-a11y="true"></script>
    <title>Starticket</title>
</head>

<body>
    <div id="titleContainer" class="container-fluid">
        <div id="mainRow" class="d-row d-flex align-items-center justify-content-center">
            <div class="col-4 h-100">
                <a href="notification.php">
                    <em id="bell" class="fas fa-bell"></em>
                    <span id="num_not" class="badge">
                        <?php if (isUserLoggedIn() && count($templateParams["notifiche"]) > 0) {
                            echo count($templateParams["notifiche"]);
                        } else {
                            echo "";
                        } ?>
                    </span>
                </a>
            </div>
            <div class="col-4">
                <a href="index.php">
                    <img id="siteLogo" src="images/logo.png" class="img-fluid" alt="Site logo">
                </a>
            </div>
            <div class="col-4 d-flex align-items-center justify-content-around ">
                <a href="login.php">
                    <em id="user" class="fas fa-user"></em>
                </a>
                <a href="cart.php">
                    <em class="fas fa-shopping-cart"></em>
                    <span class="badge">
                        <?php if (!isCartEmpty() && count($_SESSION["carrello"]) > 0) {
                            echo (count($_SESSION["carrello"]));
                        } else {
                            echo "";
                        } ?>
                    </span>
                </a>
            </div>
        </div>
        <div class="d-row d-flex">
            <div class="col h-100">
            </div>
            <div class="col-4">
            </div>
            <div class="col d-flex justify-content-center">
                <p id="welcome"><?php if (isUserLoggedIn()) {
                                    echo "Ciao ";
                                    echo $_SESSION["nome"];
                                } ?></p>
            </div>
        </div>
    </div>

    <div id="searchBarContainer" class="container-fluid justify-content-center">
        <div class="row">
            <div class="col"></div>
            <div class="col-8 col-sm-8 col-md-6 col-lg-4 col-xl-4">
                <div id="searchForm" class="d-flex justify-content-between">
                    <label for="search" class="sr-only">Search</label>
                    <input id="search" class="form-control mr-sm-2" type="search" placeholder="Cerca un'evento..." aria-label="Search">
                    <button id="lens" type="submit">
                        <em class="fas fa-search lens"></em>
                    </button>
                </div>
            </div>
            <div class="col"></div>
        </div>
    </div>

    <nav id="menuContainer" class="navbar navbar-expand navbar-light">
        <div class="navbar-collapse justify-content-around" id="topNavbar">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <?php foreach ($templateParams["categorie"] as $categoria) : ?>
                    <a class="nav-item nav-link" href="event_list_selector.php?categoria=<?php echo ($categoria["idcategoria"]) ?>&pagina=1"><?php echo ($categoria["nome"]) ?></a>
                <?php endforeach; ?>
            </div>
        </div>
    </nav>

    <main>

        <!-- Loading a specific page that uses this base template -->
        <?php
        if (isset($templateParams["pagina"])) {
            require($templateParams["pagina"]);
        }
        ?>
    </main>

    <footer id="footerContainer" class="container-fluid">
        <nav id="footer-nav">
            <div class="row">
                <div class="col">
                    <img id="footerLogo" src="images/logo.png" class="img" alt="Site logo">
                </div>
                <div class="col-xs-10 col-sm-10 col-md-10">
                    <div class="d-flex allign-items-center justify-content-around flex-direction: row">
                        <p><a href="footer_page_selector.php?page=chi_siamo" target="_self">chi siamo</a></p>
                        <p><a href="footer_page_selector.php?page=privacy" target="_self">privacy</a></p>
                        <p><a href="footer_page_selector.php?page=cookie" target="_self">cookie</a></p>
                        <p><a href="footer_page_selector.php?page=condizioni_generali" target="_self">condizioni generali</a></p>
                        <p><a href="footer_page_selector.php?page=contatti" target="_self">contatti</a></p>
                    </div>
                </div>
                <div class="col">
                </div>
            </div>
        </nav>
    </footer>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <!-- Loading js scripts -->
    <?php
    $templateParams["jsBase"] = array("js/jquery-3.4.1.min.js", "js/base.js");
    if (isset($templateParams["jsBase"])) :
        foreach ($templateParams["jsBase"] as $script) :
    ?>
    <script src="<?php echo $script; ?>"></script>
    <?php
        endforeach;
    endif;
    ?>
    
</body>

</html>