<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/executive_page.css">

<div class="container-fluid">
    <div class="row">
        <div class="col-3">
            <a href="executive_login.php" class="btn btn-secondary">Torna al menu</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-1"></div>
        <div class="col-md-8 col-sm-10">
            <?php if (count($templateParams["organizzatori"]) == 0) : ?>
                <div>
                    <h3>Non sono presenti organizzatori da mostrare in questa sezione</h3>
                </div>
            <?php else : ?>
                <h3>Organizzatori</h3>
                <?php foreach ($templateParams["organizzatori"] as $organizzatore) : ?>
                    <div class="info">
                        <ul class="list-group">
                            <li class="list-group-item">
                                Dati personali: <br> <?php echo $organizzatore["nome"] ?>
                                <br> <?php echo $organizzatore["cognome"] ?> <br> <?php echo $organizzatore["indirizzo"];
                                                                                    echo ",  ";
                                                                                    echo $organizzatore["citta"];
                                                                                    echo ",   ";
                                                                                    echo $organizzatore["cap"];
                                                                                    echo ",   ";
                                                                                    echo $organizzatore["stato"];
                                                                                    echo ",   ";
                                                                                    echo $organizzatore["Piva"] ?>
                            </li>
                            <li class="list-group-item d-flex justify-content-between">
                                Attivazione Account: <?php echo getState($organizzatore["attivo"]) ?> <?php if ($organizzatore["attivo"] == 1) : ?>
                                    <em id="<?php echo $organizzatore["id"]; ?>" class="fas fa-user-slash orgSlash"></em>
                                <?php else : ?>
                                    <em id="<?php echo $organizzatore["id"]; ?>" class="fas fa-user-plus orgPlus"></em>
                                <?php endif; ?>
                            </li>
                        </ul>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="col-md-2 col-sm-1"></div>
    </div>
</div>

<!-- Loading js scripts -->
<?php
if (isset($templateParams["js"])) :
    foreach ($templateParams["js"] as $script) :
?>
        <script src="<?php echo $script; ?>"></script>
<?php
    endforeach;
endif;
?>