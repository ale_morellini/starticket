<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/home.css">

<!-- Carousel, just three images that changes their content. Used to show main events in home page-->
<div id="carosello" class="container-fluid">
    <div class="d-row d-flex justify-content-center  align-items-center">
        <button id="carosello_left" class="btn"><em class="fas fa-chevron-left"></em></button>
        <div class="col-12 col-sm-8 col-md-4 col-lg-4">   
            <a id="0" href="">
                <img id="slide_0" class="slide w-100" src="" alt=""> <!-- no alt but'll loaded via Js-->
            </a>
        </div>
        <div class="col-12 col-sm-8 col-md-4 col-lg-4 ">
            <a id="1" href="">
                <img id="slide_1" class="slide w-100" src="" alt="">
            </a>
        </div>
        <div class="col-12 col-sm-8 col-md-4 col-lg-4 ">
            <a id="2" href="">
                <img id="slide_2" class="slide w-100" src="" alt="">
            </a>
        </div>
        <button id="carosello_right" class="btn"><em class="fas fa-chevron-right"></em></button>
    </div>
</div>


<!--Highliths section-->
<section>
    <h4>Highlihts</h4>
    <div class="container-fluid scrollmenu">
        <div class="d-row d-flex ">
            <?php foreach ($templateParams["highlights"] as $highlight) : ?>
                <a href="selected_event.php?event=<?php echo $highlight["idevento"]; ?>"> 
                <div class="d-col">
                    <div class="card">
                        <img src="<?php echo UPLOAD_DIR . $highlight["imgevento"]; ?>" class="card-img-top" alt="<?php echo $highlight["titoloevento"]; ?>">
                        <div class="card-body">
                            <h5 class="card-title text-truncate"><?php echo $highlight["titoloevento"]; ?></h5>
                            <p class="card-text text-truncate"><?php echo $highlight["artistaevento"]; ?></p>
                            
                        </div>
                    </div>
                </div>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<!-- Loading js scripts -->
<?php
if (isset($templateParams["js"])) :
    foreach ($templateParams["js"] as $script) :
?>
        <script src="<?php echo $script; ?>"></script>
<?php
    endforeach;
endif;
?>