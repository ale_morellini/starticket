<!-- Css -->
<link rel="stylesheet" type="text/css" href="css/executive_page.css">

<div class="container-fluid">
    <div class="row">
        <div id="logout_div" class="col-3">
            <a href="executive_login.php" class="btn btn-secondary">Torna al menu</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-1"></div>
        <div class="col-md-8 col-sm-10">
            <?php if (count($templateParams["clienti"]) == 0) : ?>
                <div>
                    <h3>Non sono presenti clienti da mostrare in questa sezione</h3>
                </div>
            <?php else : ?>
                <h3>Clienti</h3>
                <?php foreach ($templateParams["clienti"] as $cliente) : ?>
                    <div class="info">
                        <ul class="list-group">
                            <li class="list-group-item">
                                Dati personali: <br> <?php echo $cliente["nome"] ?>
                                <br> <?php echo $cliente["cognome"] ?> <br> <?php echo $cliente["indirizzo"];
                                                                            echo ",  ";
                                                                            echo $cliente["citta"];
                                                                            echo ",   ";
                                                                            echo $cliente["cap"];
                                                                            echo ",   ";
                                                                            echo $cliente["stato"] ?>
                            </li>
                            <li class="list-group-item d-flex justify-content-between">
                                Attivazione Account: <?php echo getState($cliente["attivo"]) ?> <?php if ($cliente["attivo"] == 1) : ?>
                                    <em id="<?php echo $cliente["id"]; ?>" class="fas fa-user-slash cliSlash"></em>
                                <?php else : ?>
                                    <i id="<?php echo $cliente["id"]; ?>" class="fas fa-user-plus cliPlus"></i>
                                <?php endif; ?>
                            </li>
                        </ul>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="col-md-2 col-sm-1"></div>
    </div>
</div>

<!-- Loading js scripts -->
<?php
if (isset($templateParams["js"])) :
    foreach ($templateParams["js"] as $script) :
?>
        <script src="<?php echo $script; ?>"></script>
<?php
    endforeach;
endif;
?>