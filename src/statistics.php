<?php 
require_once 'bootstrap.php';

if(!isUserLoggedIn()){
    header("location: login.php");
}

$templateParams["js"] = array("js/jquery-3.4.1.min.js","js/statistics.js");
$templateParams["notifiche"] = $dbh->getunreadNotifications($_SESSION["email"]);
$templateParams["pagina"] = "statistics_page.php";
$templateParams["categorie"] = $dbh->getCategories();
$date = date("Y-m-d", strtotime("-7 Days"));
$templateParams["eventi"] = $dbh->getLatestEvent($date);

$templateParams["+_venduto"] = $dbh->getHighlights(1)[0];

$date = date("Y-m-d", strtotime("-4 Months"));
$templateParams["num_eventi"] = $dbh->getMostActiveOrg(1, $date)[0]["count"];
$templateParams["org_+_attivo"] = $dbh->getOrgbyMail($dbh->getMostActiveOrg(1, $date)[0]["credenziali"])[0];

$templateParams["num_biglietti"] = $dbh->getMostActiveCli(1,$date)[0]["count"];
$templateParams["cli_+_attivo"] = $dbh->getClibyMail($dbh->getMostActiveCli(1, $date)[0]["credenziali"])[0];


require 'template/base.php';


?>