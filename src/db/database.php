<?php
class DatabaseHelper
{
    private $db;

    public function __construct($servername, $username, $password, $dbname)
    {
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if ($this->db->connect_error) {
            die("Connection failed: " . $this->db->connect_error);
        }
    }

    public function getCategories()
    {
        $stmt = $this->db->prepare("SELECT * FROM categoria");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //getter for client or organizer by given email
    public function getOrgbyMail($email)
    {
        $query = "SELECT idorganizzatore AS id, email, nome, cognome, indirizzo, citta, cap, stato, piva, tipologia, newsletter, attivo FROM organizzatore, credenziali WHERE email = credenziali AND email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getClibyMail($email)
    {
        $query = "SELECT  idcliente AS id, email, nome, cognome, indirizzo, citta, cap, stato, tipologia, newsletter, attivo FROM cliente, credenziali WHERE email = credenziali AND email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //returns user's mail that allows to receive newsletter
    public function getCliSubscribed()
    {
        $query = "SELECT email FROM credenziali WHERE newsletter = 1";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAmmbyMail($email)
    {
        $query = "SELECT idamministratore As id, email, nome, cognome, indirizzo, tipologia FROM amministratore, credenziali WHERE email = credenziali AND email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    // if $val = 0 this query return the banned client otherwise the active ones
    public function getActivatedClient($val)
    {
        $query = "SELECT  idcliente AS id, nome, cognome, indirizzo, citta, cap, stato, attivo FROM cliente WHERE attivo = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $val);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    // if $val = 0 this query return the inactive organizer otherwise the active ones
    public function getActivatedOrganizer($val)
    {
        $query = "SELECT  idorganizzatore AS id, nome, cognome, indirizzo, citta, cap, stato, Piva, attivo FROM organizzatore WHERE attivo = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $val);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }


    // getter for event by given category
    public function getEventByCategory($idcategoria)
    {
        $query = "SELECT * FROM evento WHERE categoria=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idcategoria);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }


    public function getEventById($id)
    {
        $query = "SELECT * FROM evento WHERE idevento =?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventAuthorById($id)
    {
        $query = "SELECT idevento AS id, titoloevento, dataevento, oraevento, credenziali FROM evento, organizzatore WHERE idorganizzatore = organizzatore AND idevento =?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventByOrg($idorganizzatore)
    {
        $query = "SELECT idevento AS id, titoloevento, artistaevento, luogoevento, dataevento, oraevento, costoevento, infoevento, imgevento FROM evento, organizzatore WHERE organizzatore = idorganizzatore AND idorganizzatore=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $idorganizzatore);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }


    public function getNextEvent($idorganizzatore)
    {
        $query = "SELECT idevento AS id, titoloevento, artistaevento, luogoevento, dataevento, oraevento, costoevento, infoevento, imgevento FROM evento, organizzatore WHERE organizzatore = idorganizzatore  AND idorganizzatore=? AND dataevento >= CURRENT_DATE ORDER BY dataevento DESC";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $idorganizzatore);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getOldEvent($idorganizzatore)
    {
        $query = "SELECT idevento AS id, titoloevento, artistaevento, luogoevento, dataevento, oraevento, costoevento, infoevento, imgevento FROM evento, organizzatore WHERE organizzatore = idorganizzatore  AND idorganizzatore=? AND dataevento < CURRENT_DATE ORDER BY dataevento DESC";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $idorganizzatore);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNextTicket($idcliente)
    {
        $query = "SELECT idevento AS id, titoloevento, artistaevento, luogoevento, dataevento, oraevento, costoevento, infoevento, imgevento, databiglietto FROM evento, biglietto, cliente WHERE cliente = idcliente AND evento = idevento  AND idcliente=? AND dataevento >= CURRENT_DATE ORDER BY dataevento DESC";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $idcliente);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getOldTicket($idcliente)
    {
        $query = "SELECT idevento AS id, titoloevento, artistaevento, luogoevento, dataevento, oraevento, costoevento, infoevento, imgevento, databiglietto FROM evento, biglietto, cliente WHERE cliente = idcliente AND evento = idevento  AND idcliente=? AND dataevento < CURRENT_DATE ORDER BY dataevento DESC";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $idcliente);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getTicketBought($idevento)
    {
        $query = "SELECT credenziali FROM biglietto, cliente WHERE cliente = idcliente  AND evento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $idevento);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEvent()
    {
        $query = "SELECT * FROM evento";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getLatestEvent($date)
    {
        $query = "SELECT * FROM evento WHERE datainserimento >= ? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $date);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getHighlights($max)
    {
        $query = "SELECT *, sum(posti) as count FROM evento, biglietto WHERE evento.idevento = evento GROUP BY evento ORDER BY sum(posti) DESC LIMIT ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $max);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getMostActiveOrg($max, $date)
    {
        $query = "SELECT credenziali, COUNT(idorganizzatore) AS count FROM evento, organizzatore WHERE idorganizzatore = organizzatore AND datainserimento >= ? GROUP BY idorganizzatore ORDER BY COUNT(idorganizzatore) DESC LIMIT ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si', $date, $max);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getMostActiveCli($max, $date)
    {
        $query = "SELECT credenziali, SUM(posti) AS count FROM biglietto, cliente WHERE idcliente = cliente AND databiglietto >= ? GROUP BY cliente ORDER BY sum(posti) DESC LIMIT ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si', $date, $max);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNotifications($email)
    {
        $query = "SELECT idnotifica, titolo, descrizione, datanotifica, letto FROM notifica WHERE credenziali = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getUnreadNotifications($email)
    {
        $letto = 0;
        $query = "SELECT idnotifica FROM notifica WHERE credenziali = ? AND letto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si', $email, $letto);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNotificationsById($id)
    {
        $query = "SELECT titolo, anteprima, descrizione, datanotifica, letto FROM notifica WHERE idnotifica = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }


    public function getCredential($email)
    {
        $query = "SELECT pwd, tipologia, newsletter FROM `credenziali` WHERE email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertCategory($category)
    {
        $query = "INSERT INTO categoria (nome) VALUES (?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $category);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function insertEvent($titoloevento, $artistaevento, $luogoevento, $cittaevento, $dataevento, $oraevento, $numeroposti, $costoevento, $infoevento, $imgevento, $organizzatore, $categoria)
    {
        $datainserimento = date("Y-m-d");
        $query = "INSERT INTO evento (titoloevento, artistaevento, luogoevento, cittaevento, dataevento, oraevento, numeroposti, costoevento, infoevento, imgevento, datainserimento, organizzatore, categoria) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssssssidsssii', $titoloevento, $artistaevento, $luogoevento, $cittaevento, $dataevento, $oraevento, $numeroposti, $costoevento, $infoevento, $imgevento, $datainserimento, $organizzatore, $categoria);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function insertNotification($titolo, $descrizione, $data, $letto, $credenziali)
    {
        $query = "INSERT INTO notifica(titolo, descrizione, datanotifica, letto, credenziali) VALUES (?,?,?,?,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sssis', $titolo, $descrizione, $data, $letto, $credenziali);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function updateEventOfAuthor($idevento, $titoloevento, $artistaevento, $luogoevento, $dataevento, $oraevento, $numeroposti, $costoevento, $infoevento, $imgevento, $organizzatore, $categoria)
    {
        $query = "UPDATE evento SET titoloevento = ?, artistaevento = ?, luogoevento = ?, dataevento = ?, oraevento = ?, numeroposti = ?, costoevento = ?, infoevento = ?, imgevento = ?, categoria = ? WHERE idevento = ? AND organizzatore = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sssssidssiii', $titoloevento, $artistaevento, $luogoevento, $dataevento, $oraevento, $numeroposti, $costoevento, $infoevento, $imgevento, $categoria, $idevento, $organizzatore);

        return $stmt->execute();
    }

    public function getEventSeats($idevento)
    {
        $query = "SELECT numeroposti FROM evento WHERE idevento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idevento);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function updateEventSeats($idevento, $numeroposti)
    {
        $query = "UPDATE evento SET numeroposti = ? WHERE idevento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $numeroposti, $idevento);

        return $stmt->execute();
    }

    public function updateClient($nome, $cognome, $indirzzo, $città, $cap, $stato, $email)
    {
        $query = "UPDATE cliente SET nome = ?, cognome = ?, indirizzo = ?, citta = ?, CAP = ?, stato = ? WHERE credenziali = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sssssss', $nome, $cognome, $indirzzo, $città, $cap, $stato, $email);

        return $stmt->execute();
    }

    public function updateOrganizer($nome, $cognome, $indirzzo, $città, $cap, $stato, $piva, $email)
    {
        $query = "UPDATE cliente SET nome = ?, cognome = ?, indirizzo = ?, citta = ?, CAP = ?, stato = ?, piva = ? WHERE credenziali = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssssssss', $nome, $cognome, $indirzzo, $città, $cap, $stato, $piva, $email);

        return $stmt->execute();
    }

    public function updatePwd($password, $email)
    {
        $query = "UPDATE credenziali SET pwd = ? WHERE email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $password, $email);

        return $stmt->execute();
    }

    public function updateNewsletter($val, $email)
    {
        $query = "UPDATE credenziali SET newsletter = ? WHERE email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $val, $email);

        return $stmt->execute();
    }


    public function insertClient($nome, $cognome, $indirizzo, $citta, $CAP, $stato, $credenziali)
    {
        $query = "INSERT INTO cliente (nome, cognome, indirizzo, citta, CAP, stato, credenziali) VALUES (?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sssssss', $nome, $cognome, $indirizzo, $citta, $CAP, $stato, $credenziali);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function insertOrganizer($nome, $cognome, $indirizzo, $citta, $CAP, $stato, $Piva, $credenziali)
    {
        $query = "INSERT INTO organizzatore (nome, cognome, indirizzo, citta, CAP, stato, Piva, credenziali) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssssssss', $nome, $cognome, $indirizzo, $citta, $CAP, $stato, $Piva, $credenziali);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function insertTicket($idCliente, $idEvento, $posti)
    {
        $data = date("Y-m-d");
        $query = "INSERT INTO biglietto (evento, cliente, posti, databiglietto) VALUES (?,?,?,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('iiis', $idEvento, $idCliente, $posti, $data);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function insertCredentials($email, $pwd, $tipologia, $news)
    {
        $query = "INSERT INTO credenziali (email, pwd, tipologia, newsletter) VALUES (?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sssi', $email, $pwd, $tipologia, $news);

        return $stmt->execute();
    }

    public function setClientAttivation($id, $state)
    {
        $query = "UPDATE cliente SET attivo = ? WHERE idcliente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $state, $id);

        return $stmt->execute();
    }

    public function setOrganizerAttivation($id, $state)
    {
        $query = "UPDATE organizzatore SET attivo = ? WHERE idorganizzatore = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $state, $id);

        return $stmt->execute();
    }

    public function setNotificationRead($id, $letto)
    {
        $query = "UPDATE notifica SET letto = ? WHERE idnotifica = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $letto, $id);

        return $stmt->execute();
    }

    public function deleteEvent($idevento)
    {
        $query = "DELETE FROM evento WHERE idevento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idevento);
        return $stmt->execute();
    }

    public function deleteTickets($idevento)
    {
        $query = "DELETE FROM biglietto WHERE evento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idevento);
        return $stmt->execute();
    }

    public function deleteCategory($id)
    {
        $query = "DELETE FROM categoria WHERE idcategoria = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        return $stmt->execute();
    }

    public function deleteNotification($id)
    {
        $query = "DELETE FROM notifica WHERE idnotifica = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        return $stmt->execute();
    }

    public function locate($string, $substring)
    {
        $query = "SELECT LOCATE(? ,? ) AS pos";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $substring, $string);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
}
