<?php
//Cart manager, can add events to cart such as perform cart checkout

require_once 'bootstrap.php';

$type = $_POST["type"];



switch ($type) {

    case "add":
        if (empty($_SESSION["carrello"])) {
            $a = explode(" ", $_POST["id"]);
            $_SESSION["carrello"] = array($a[0] => 1);
        } else {
            $a = explode(" ", $_POST["id"]);
            //Updating number of events in case user has already insert the same one in the cart
            if (array_key_exists($a[0], $_SESSION["carrello"])) {
                $val = $_SESSION["carrello"][$a[0]];
                $_SESSION["carrello"][$a[0]] = $val + 1;
            } else {
                $_SESSION["carrello"] += [$a[0] => 1];
            }
        }

        echo (count($_SESSION["carrello"]));
        break;

    case "updateQuantity":
        $_SESSION["carrello"][$_POST["id"]] = $_POST["quantity"];
        echo ("ok");
        break;

    case "trash":
        unset($_SESSION["carrello"][$_POST["id"]]);
        echo ("ok");
        break;

    case "buy":

        if (!isUserLoggedIn()) {
            echo ("notLogged");
        } else {
            echo ("ok");
        }
        break;

    case "checkout":

        //Just creating tickets in DB
        $idCliente = $_SESSION["id"];
        foreach ($_SESSION["carrello"] as $idEvento => $quantity) :
            $dbh->insertTicket($idCliente, $idEvento, $quantity);
            $result = $dbh->getEventSeats($idEvento);
            $numposti = $result[0]["numeroposti"];
            $numposti -= $quantity;
            $dbh->updateEventSeats($idEvento, $numposti);
            if ($dbh->getEventSeats($idEvento)[0]["numeroposti"] == 0) {
                soldOutNotification($dbh, $idEvento);
            }
        endforeach;

        //At this point events need to be removed from cart
        unset($_SESSION["carrello"]);
        echo ("ok");
        break;

    default:
        echo "nothing done";
}
