<?php
require_once 'bootstrap.php';


if(isset($_GET["msg"])){
    if($_GET["msg"] == "logout"){
        session_unset();
        header("location: login.php");
    }
}

if(isUserLoggedIn()){
    $templateParams["notifiche"] = $dbh->getUnreadNotifications($_SESSION["email"]);
}else{
    $templateParams["notifiche"] = "";
}

//Base Template
$templateParams["titolo"] = "Starticket";
$templateParams["pagina"] = "home.php";
$templateParams["js"] = array("js/jquery-3.4.1.min.js","js/events.js");
$templateParams["jsBase"] = array("js/jquery-3.4.1.min.js","js/base.js");
$templateParams["categorie"] = $dbh->getCategories();
$templateParams["highlights"] = $dbh->getHighlights(6);
$templateParams["eventi"] = $dbh->getEvent();

require 'template/base.php';
