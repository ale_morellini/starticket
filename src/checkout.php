<?php
require_once 'bootstrap.php';

//Base Template
$templateParams["titolo"] = "Starticket";
$templateParams["categorie"] = $dbh->getCategories();
$templateParams["pagina"] = "checkout_page.php";
$templateParams["js"] = array("js/jquery-3.4.1.min.js","js/checkout.js");
if(isUserLoggedIn()){
    $templateParams["notifiche"] = $dbh->getUnreadNotifications($_SESSION["email"]);
}else{
    $templateParams["notifiche"] = "";
}

require 'template/base.php';
?>