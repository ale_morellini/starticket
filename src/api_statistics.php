<?php
require_once 'bootstrap.php';

if(isset($_POST["sendNews"]) && $_POST["sendNews"] == 1){
    
    $date = date("Y-m-d", strtotime("-7 Days")) ;
    $users = $dbh->getCliSubscribed();
    $events = $dbh->getLatestEvent($date);

    foreach ($users as $email) :
        sendNewsNotification($dbh, $events, $email["email"]);
    endforeach;
    echo "ok";
}

if (isset($_POST["cli"]) && $_POST["cli"] == 1) {

    $email = $_POST["email"];
    $result = contactClient($dbh, $email);
    echo $result;
}

if (isset($_POST["org"]) && $_POST["org"] == 1) {

    $email = $_POST["email"];
    $result = contactOrganizer($dbh, $email);
    echo $result;
}

?>