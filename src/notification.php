<?php 
require_once 'bootstrap.php';

if(!isUserLoggedIn()){
    header("location: login.php");
}

$templateParams["js"] = array("js/jquery-3.4.1.min.js","js/notification.js");
$templateParams["notifiche_lette"] = $dbh->getNotifications($_SESSION["email"]);
$templateParams["notifiche"] = $dbh->getunreadNotifications($_SESSION["email"]);
$templateParams["pagina"] = "notifications_page.php";
$templateParams["categorie"] = $dbh->getCategories();

require 'template/base.php';


?>