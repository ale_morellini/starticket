<?php
require_once 'bootstrap.php';



if(isUserLoggedIn()){
    $templateParams["notifiche"] = $dbh->getunreadNotifications($_SESSION["email"]);
}else{
    $templateParams["notifiche"] = "";
}
$templateParams["pagina"] = "registration_form.php";
$templateParams["categorie"] = $dbh->getCategories();
session_unset();


function checkForm()
{

    if (!isset($_POST["email"]) || !isset($_POST["password"]) || !isset($_POST["nome"]) || !isset($_POST["cognome"]) || !isset($_POST["indirizzo"]) || !isset($_POST["città"]) || !isset($_POST["cap"]) || !isset($_POST["stato"])) {
        return false;
    }
    return true;
};

function checkOrganizzatore()
{
    if (isset($_POST["checkbox"])) {
        return true;
    }
    return false;
};

function inserimento($dbh, $tipologia)
{
    if (isset($_POST["news_checkbox"])) {
        $newsletter = 1;
    } else {
        $newsletter = 0;
    }
    $password = $_POST["password"];
    $hash = password_hash($password, PASSWORD_BCRYPT);
    $result = $dbh->insertCredentials($_POST["email"], $hash, $tipologia, $newsletter);
    if ($result != false) {
        if ($tipologia == "organizzatore") {
            $id = $dbh->insertOrganizer($_POST["nome"], $_POST["cognome"], $_POST["indirizzo"], $_POST["città"], $_POST["cap"], $_POST["stato"], $_POST["piva"], $_POST["email"]);
            if ($id != false) {
                return true;
            }
        } else {
            $id = $dbh->insertClient($_POST["nome"], $_POST["cognome"], $_POST["indirizzo"], $_POST["città"], $_POST["cap"], $_POST["stato"], $_POST["email"]);
            if ($id != false) {
                return true;
            }
        }
    }
    return false;
};

function feedback($result)
{
    if ($result) {
        $templateParams["formmsg"] = "Registrazione avvenuta con successo";
        header("location: login.php");
    } else {
        $templateParams["formmsg"] = "Errore in fase di registrazione. Riprovare";
        header("location: registration.php");
    }
};



if (checkForm()) {
    $result = $dbh->getCredential($_POST["email"]);

    if (count($result) == 0) {
        if (checkOrganizzatore()) {
            $result = inserimento($dbh, "organizzatore");
            if (true) {
                newOrgNotification($dbh);
            }
            feedback($result);
        } else {
            $result = inserimento($dbh, "cliente");
            feedback($result);
        }
    } else {
        feedback(false);
    }
}


require 'template/base.php';
