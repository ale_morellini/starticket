<?php
require_once 'bootstrap.php';

if(!isUserLoggedIn() || !isset($_GET["action"]) || ($_GET["action"]!=1 && $_GET["action"]!=2 && $_GET["action"]!=3) || ($_GET["action"]!=1 && !isset($_GET["id"]))){
    header("location: login.php");
}

if($_GET["action"]!=1){
    $risultato = $dbh->getEventById($_GET["id"]);
    if(count($risultato)==0){
        $templateParams["evento"] = null;
    }
    else{
        $templateParams["evento"] = $risultato[0];
    }
}
else{
    $templateParams["evento"] = getEmptyEvent();
}


$templateParams["notifiche"] = $dbh->getUnreadNotifications($_SESSION["email"]);
$templateParams["titolo"] = "Starticket - Event manager";
$templateParams["pagina"] = "admin_form.php";
$templateParams["categorie"] = $dbh->getCategories();

$templateParams["azione"] = $_GET["action"];

require 'template/base.php';
?>