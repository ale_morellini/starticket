<?php
require_once 'bootstrap.php';
$eventi = $dbh->getEvent();

for($i = 0; $i < count($eventi); $i++){
    $eventi[$i]["imgevento"] = UPLOAD_DIR.$eventi[$i]["imgevento"];
}
header('Content-Type: application/json');
echo json_encode($eventi);
?>