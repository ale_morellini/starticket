-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema starticketicket
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema starticketicket
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `starticket` DEFAULT CHARACTER SET utf8 ;
USE `starticket` ;


-- -----------------------------------------------------
-- Table `starticket`.`credenziali`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `starticket`.`credenziali` (
  `email` VARCHAR(100) NOT NULL,
  `pwd` VARCHAR(100) NOT NULL,
  `tipologia` VARCHAR(100) NOT NULL,
  `newsletter` TINYINT NOT NULL,
  PRIMARY KEY (`email`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `starticket`.`categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `starticket`.`categoria` (
  `idcategoria` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`idcategoria`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `starticket`.`notifica`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `starticket`.`notifica` (
  `idnotifica` INT NOT NULL AUTO_INCREMENT,
  `titolo` VARCHAR(500) NOT NULL,
  `descrizione` VARCHAR(500) NOT NULL,
  `datanotifica` DATE NOT NULL,
  `letto` TINYINT NULL DEFAULT 0,
  `credenziali` VARCHAR(100) NOT NULL, 
  PRIMARY KEY (`idnotifica`),
  CONSTRAINT `fk_notifica_credenziali`
	FOREIGN KEY (`credenziali`)
    REFERENCES `starticket`.`credenziali` (`email`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `starticket`.`organizzatore`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `starticket`.`organizzatore` (
  `idorganizzatore` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `cognome` VARCHAR(45) NOT NULL,
  `indirizzo` VARCHAR(45) NOT NULL,
  `citta` VARCHAR(45) NOT NULL,
  `CAP` VARCHAR(6) NOT NULL,
  `stato` VARCHAR(45) NOT NULL,
  `Piva` VARCHAR(45) NOT NULL,
  `credenziali` VARCHAR(100) NOT NULL,
  `attivo` TINYINT NULL DEFAULT 0,
  PRIMARY KEY (`idorganizzatore`),
  CONSTRAINT `fk_organizzatore_credenziali`
	FOREIGN KEY (`credenziali`)
    REFERENCES `starticket`.`credenziali` (`email`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `starticket`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `starticket`.`cliente` (
  `idcliente` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `cognome` VARCHAR(45) NOT NULL,
  `indirizzo` VARCHAR(45) NOT NULL,
  `citta` VARCHAR(45) NOT NULL,
  `CAP` VARCHAR(6) NOT NULL,
  `stato` VARCHAR(45) NOT NULL,
  `credenziali` VARCHAR(100) NOT NULL,
  `attivo` TINYINT DEFAULT 1,
  PRIMARY KEY (`idcliente`),
  CONSTRAINT `fk_cliente_credenziali`
	FOREIGN KEY (`credenziali`)
    REFERENCES `starticket`.`credenziali` (`email`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `starticket`.`amministratore`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `starticket`.`amministratore` (
  `idamministratore` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `cognome` VARCHAR(45) NOT NULL,
  `indirizzo` VARCHAR(45) NOT NULL,
  `citta` VARCHAR(45) NOT NULL,
  `CAP` VARCHAR(6) NOT NULL,
  `credenziali` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`idamministratore`),
  CONSTRAINT `fk_amministratore_credenziali`
	FOREIGN KEY (`credenziali`)
    REFERENCES `starticket`.`credenziali` (`email`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `starticket`.`evento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `starticket`.`evento` (
  `idevento` INT NOT NULL AUTO_INCREMENT,
  `titoloevento` VARCHAR(100) NOT NULL,
  `artistaevento` VARCHAR(100) NOT NULL,
  `luogoevento` VARCHAR(100) NOT NULL,
  `cittaevento` VARCHAR(45) NOT NULL,
  `dataevento` DATE NOT NULL,
  `oraevento` TIME NOT NULL,
  `numeroposti` INT NOT NULL,
  `costoevento` DOUBLE NOT NULL,
  `infoevento` VARCHAR(1000) NOT NULL,
  `imgevento` VARCHAR(100) NOT NULL,
  `datainserimento` DATE NOT NULL,
  `organizzatore` INT NOT NULL,
  `categoria` INT NOT NULL,
  PRIMARY KEY (`idevento`),
  CONSTRAINT `fk_evento_organizzatore`
  	FOREIGN KEY (`organizzatore`)
    REFERENCES `starticket`.`organizzatore` (`idorganizzatore`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_evento_categoria`
  	FOREIGN KEY (`categoria`)
    REFERENCES `starticket`.`categoria` (`idcategoria`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)

ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `starticket`.`biglietto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `starticket`.`biglietto` (
  `idbiglietto` INT NOT NULL AUTO_INCREMENT,
  `evento` INT NOT NULL,
  `cliente` INT NOT NULL,
  `posti` INT NOT NULL,
  `databiglietto` DATE NOT NULL,
  PRIMARY KEY (`idbiglietto`),
  CONSTRAINT `fk_biglietto_cliente`
    FOREIGN KEY (`cliente`)
    REFERENCES `starticket`.`cliente` (`idcliente`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biglietto_evento`
	FOREIGN KEY (`evento`)
    REFERENCES `starticket`.`evento` (`idevento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;





SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


ALTER TABLE `starticket`.`amministratore` DROP INDEX `fk_amministratore_credenziali`, ADD UNIQUE `fk_amministratore_credenziali` (`credenziali`) USING BTREE; 
ALTER TABLE `starticket`.`cliente` DROP INDEX `fk_cliente_credenziali`, ADD UNIQUE `fk_cliente_credenziali` (`credenziali`) USING BTREE; 
ALTER TABLE `starticket`.`organizzatore` DROP INDEX `fk_organizzatore_credenziali`, ADD UNIQUE `fk_organizzatore_credenziali` (`credenziali`) USING BTREE; 
