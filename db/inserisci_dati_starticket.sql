-- -----------------------------------------------------
-- Inserimento credenziali
-- -----------------------------------------------------
INSERT INTO `credenziali`(`email`, `pwd`, `tipologia`,`newsletter`) VALUES ("amministratore1@starticket.com", "$2y$10$rl0HpOY8YQPvoiNVUmry2uD94ZmxbkXnFGqAgg5P390Yi1la3CIJS", "amministratore", 0);

INSERT INTO `credenziali`(`email`, `pwd`, `tipologia`,`newsletter`) VALUES ("organizzatore1@starticket.com", "$2y$10$rl0HpOY8YQPvoiNVUmry2uD94ZmxbkXnFGqAgg5P390Yi1la3CIJS", "organizzatore", 0);
INSERT INTO `credenziali`(`email`, `pwd`, `tipologia`,`newsletter`) VALUES ("organizzatore2@starticket.com", "$2y$10$rl0HpOY8YQPvoiNVUmry2uD94ZmxbkXnFGqAgg5P390Yi1la3CIJS", "organizzatore", 0);
INSERT INTO `credenziali`(`email`, `pwd`, `tipologia`,`newsletter`) VALUES ("organizzatore3@starticket.com", "$2y$10$rl0HpOY8YQPvoiNVUmry2uD94ZmxbkXnFGqAgg5P390Yi1la3CIJS", "organizzatore", 0);

INSERT INTO `credenziali`(`email`, `pwd`, `tipologia`,`newsletter`) VALUES ("cliente1@starticket.com", "$2y$10$rl0HpOY8YQPvoiNVUmry2uD94ZmxbkXnFGqAgg5P390Yi1la3CIJS","cliente", 0);
INSERT INTO `credenziali`(`email`, `pwd`, `tipologia`,`newsletter`) VALUES ("cliente2@starticket.com", "$2y$10$rl0HpOY8YQPvoiNVUmry2uD94ZmxbkXnFGqAgg5P390Yi1la3CIJS", "cliente", 1);
INSERT INTO `credenziali`(`email`, `pwd`, `tipologia`,`newsletter`) VALUES ("cliente3@starticket.com", "$2y$10$rl0HpOY8YQPvoiNVUmry2uD94ZmxbkXnFGqAgg5P390Yi1la3CIJS", "cliente", 1);
INSERT INTO `credenziali`(`email`, `pwd`, `tipologia`,`newsletter`) VALUES ("cliente4@starticket.com", "$2y$10$rl0HpOY8YQPvoiNVUmry2uD94ZmxbkXnFGqAgg5P390Yi1la3CIJS", "cliente", 1);
INSERT INTO `credenziali`(`email`, `pwd`, `tipologia`,`newsletter`) VALUES ("cliente5@starticket.com", "$2y$10$rl0HpOY8YQPvoiNVUmry2uD94ZmxbkXnFGqAgg5P390Yi1la3CIJS", "cliente", 1);
INSERT INTO `credenziali`(`email`, `pwd`, `tipologia`,`newsletter`) VALUES ("cliente6@starticket.com", "$2y$10$rl0HpOY8YQPvoiNVUmry2uD94ZmxbkXnFGqAgg5P390Yi1la3CIJS", "cliente", 1);

-- -----------------------------------------------------
-- Inserimento Organizzatore
-- -----------------------------------------------------
ALTER TABLE `organizzatore`
  MODIFY `idorganizzatore` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
  
INSERT INTO `organizzatore`(`nome`, `cognome`, `indirizzo`, `citta`, `CAP`, `stato`, `Piva`, `credenziali`) VALUES ("Mario", "Rossi", "via Roma 1", "Roma", "00100", "Italia", "123456789", "organizzatore1@starticket.com");

INSERT INTO `organizzatore`(`nome`, `cognome`, `indirizzo`, `citta`, `CAP`, `stato`, `Piva`, `credenziali`) VALUES ("Luca", "Bianchi", "via Milano 2", "Milano", "20019", "Italia", "987654321", "organizzatore2@starticket.com");

INSERT INTO `organizzatore`(`nome`, `cognome`, `indirizzo`, `citta`, `CAP`, `stato`, `Piva`, `credenziali`) VALUES ("Achille", "Rosi", "via Ancona 22", "Ancona", "60121", "Italia", "567295874", "organizzatore3@starticket.com");

-- -----------------------------------------------------
-- Inserimento amministratore
-- -----------------------------------------------------
ALTER TABLE `amministratore`
  MODIFY `idamministratore` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
  
INSERT INTO `amministratore`(`nome`, `cognome`, `indirizzo`, `citta`, `CAP`, `credenziali`) VALUES ("Fabio", "Verdi", "via Lucca 1", "lucca", "55100", "amministratore1@starticket.com");

-- -----------------------------------------------------
-- Inserimento Cliente
-- -----------------------------------------------------    
ALTER TABLE `cliente`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
  
  INSERT INTO `cliente`(`nome`, `cognome`, `indirizzo`, `citta`, `CAP`, `stato`, `credenziali`) VALUES ("Giovanni", "Neri", "via Bergamo 1", "Bergamo", "24121", "Italia", "cliente1@starticket.com");

INSERT INTO `cliente`(`nome`, `cognome`, `indirizzo`, `citta`, `CAP`, `stato`, `credenziali`) VALUES ("Sara", "Neri", "via Bergamo 2", "Bergamo", "24121", "Italia", "cliente2@starticket.com");

INSERT INTO `cliente`(`nome`, `cognome`, `indirizzo`, `citta`, `CAP`, `stato`, `credenziali`) VALUES ("Laura", "Bianchi", "via Milano 15", "Milano", "20019", "Italia", "cliente3@starticket.com");

INSERT INTO `cliente`(`nome`, `cognome`, `indirizzo`, `citta`, `CAP`, `stato`, `credenziali`) VALUES ("Giovanni", "Gialli", "via Milano 88", "Milano", "20019", "Italia", "cliente4@starticket.com");

INSERT INTO `cliente`(`nome`, `cognome`, `indirizzo`, `citta`, `CAP`, `stato`, `credenziali`) VALUES ("Viola", "Aranci", "via Napoli 5", "Napoli", "80100", "Italia", "cliente5@starticket.com");

INSERT INTO `cliente`(`nome`, `cognome`, `indirizzo`, `citta`, `CAP`, `stato`, `credenziali`) VALUES ("Mara", "Bianchi", "via Napoli 76", "Napoli", "80100", "Italia", "cliente6@starticket.com");


-- -----------------------------------------------------
-- Inserimento Categoria
-- -----------------------------------------------------    
INSERT INTO `categoria`(`nome`) VALUES ("Teatro");  
INSERT INTO `categoria`(`nome`) VALUES ("Musica");  
INSERT INTO `categoria`(`nome`) VALUES ("Intrattenimento");


-- -----------------------------------------------------
-- Inserimento Evento
-- -----------------------------------------------------    
ALTER TABLE `evento`
  MODIFY `idevento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

INSERT INTO `evento`(`titoloevento`, `artistaevento`, `luogoevento`, `cittaevento`, `dataevento`, `oraevento`, `numeroposti`, `costoevento`, `infoevento`, `imgevento`, `datainserimento`, `organizzatore`, `categoria`) VALUES ("Atti osceni: Oscar Wilde", "compagnia Elfo Puccini", "Elfo Puccini", "Milano", "2020-01-23", "20:30", 10, 42.00,"Torna in scena il testo di Moisés Kaufman che racconta i tre processi che coinvolsero Oscar Wilde nel 1895. Un racconto biografico ma soprattutto un ritratto a tutto tondo della personalità artistica e umana di questo soave e impietoso fustigatore delle ipocrisie della società vittoriana, epitome di tutte le società ipocrite.
Lo spettacolo di Bruni e Frongia mette al centro della scena un'aula di tribunale, riuscendo ad aprire nel serrato dibattito giudiziario squarci poetici e incursioni commoventi nell'opera del poeta, travalicando i confini di un'appassionante ricostruzione storica per trasformarsi in un rito teatrale in cui si parla di arte, di libertà, di teatro, di sesso, di passione.
E il processo allo scrittore irlandese diventa il processo a qualunque artista proclami con forza l'assoluta anarchia della creazione.","atti.png", "2019-12-10",  1, 1);

INSERT INTO `evento`(`titoloevento`, `artistaevento`, `luogoevento`, `cittaevento`, `dataevento`, `oraevento`, `numeroposti`, `costoevento`, `infoevento`, `imgevento`, `datainserimento`, `organizzatore`, `categoria`) VALUES ("Grease", "Compagnia della rancia", "Teatro Regio", "Parma",  "2020-02-12", "21:00", 300,  50.00,"Tutti pazzi per la GREASEMANIA! In Italia, il musical di Jim Jacobs e Warren Casey, prodotto da Compagnia della Rancia con la regia di Saverio Marconi, in più di 20 anni sui palcoscenici di ogni Regione, è un fenomeno che si conferma ogni sera, con più di 1.800 repliche per oltre 1.870.000 spettatori a teatro. Una festa travolgente che dal 1997 accende le platee italiane, e ha dato il via alla musical-mania trasformandosi in un vero e proprio fenomeno di costume “pop”, un cult intergenerazionale che non è mai stato così attuale ed è amatissimo anche dalle nuove generazioni. In oltre 20 anni di successi strabilianti in Italia, GREASE IL MUSICAL si è trasformato in una macchina da applausi, cambiando il modo di vivere l’esperienza di andare a teatro. Oggi è una magia coloratissima e luminosa che si ripete ogni sera, una festa da condividere con amici e famiglie, senza riuscire a restare fermi sulle poltrone ma scatenarsi a ballare: un inno all'amicizia, agli amori indimenticabili e assoluti dell'adolescenza, oltre che a un'epoca - gli anni '50 – che oggi come allora rappresentano il simbolo di un mondo spensierato e di una fiducia incrollabile nel futuro. Si vedono tra il pubblico scatenarsi insieme almeno tre generazioni, ognuna innamorata di GREASE per un motivo differente: la nostalgia del mondo perfetto degli anni Cinquanta, i ricordi legati al film campione di incassi del 1978 con John Travolta e Olivia Newton-John (del quale è stato recentemente annunciato un prequel dal titolo “Summer Loving”) e alle indimenticabili canzoni, l’immedesimazione in una storia d’amore senza tempo, tra ciuffi ribelli modellati con la brillantina, giubbotti di pelle e sbarazzine gonne a ruota.","grease.png", "2019-10-02", 1, 1);

INSERT INTO `evento`(`titoloevento`, `artistaevento`, `luogoevento`, `cittaevento`, `dataevento`, `oraevento`, `numeroposti`, `costoevento`, `infoevento`, `imgevento`, `datainserimento`, `organizzatore`, `categoria`) VALUES ("Re Liar", "Compagnia Eliseo", "Teatro Eliseo", "Roma", "2020-07-11", "20:00", 100, 75.00,"Ed eccomi qui per la terza volta, alla mia veneranda età, impersonare Lear. Perché? Mi sono sempre sentito non all’altezza ad interpretare quel sublime crogiolo di umanità che è il personaggio di Lear. In questa mia difficile impresa mi accompagna la convinzione che per tentare di interpretare Lear non servono tanto le eventuali doti tecniche maturate nel tempo quanto la grande ricchezza umana che negli anni mi hanno regalato nel loro, a volte faticoso cammino. Spero solo che quel luogo magico che è il palcoscenico possa venire in soccorso ai nostri limiti. Cosa c’è di più poeticamente coerente di un palcoscenico per raccontare la vita? E nel Re Lear è la vita stessa che per raccontarsi ha bisogno di farsi teatro.","liar.png", CURRENT_DATE, 1, 1);

INSERT INTO `evento`(`titoloevento`, `artistaevento`, `luogoevento`, `cittaevento`, `dataevento`, `oraevento`, `numeroposti`, `costoevento`, `infoevento`, `imgevento`, `datainserimento`, `organizzatore`, `categoria`) VALUES ("Il mago di oz", "Silvio peroni", "Teatro Carignano", "Torino", "2019-10-02", "21:30", 20, 15.00,"Torna al Teatro Carignano il consueto appuntamento rivolto ai più piccoli con Il Mago di Oz per la regia di Silvio Peroni. Conosciuto anche come Il meraviglioso mago di Oz il testo è un classico della letteratura che ha incantato intere generazioni e che ha visto anche una famosissima versione cinematografica (1939) diretta da Victor Fleming con Judy Garland.","oz.png", "2019-08-22", 1, 1);

INSERT INTO `evento`(`titoloevento`, `artistaevento`, `luogoevento`, `cittaevento`, `dataevento`, `oraevento`, `numeroposti`, `costoevento`, `infoevento`, `imgevento`, `datainserimento`, `organizzatore`, `categoria`) VALUES ("Lillo & Greg - gagmen", "Lillo & Greg", "Teatro nuovo", "Ferrara", "2020-06-27", "21:00", 50, 34.00,"Un nuovo sfavillante  firmato Lillo & Greg che ripropone ulteriori cavalli di battaglia della famosa coppia comica ma questa volta tratti non soltanto dal loro repertorio teatrale, ma anche da quello televisivo e radiofonico. Una miscela esplosiva ed esilarante che finalmente porta in teatro radio e tv per una sintesi perfetta, arguta, sottile, colta e scoppiettante come solo Lillo e Greg sanno fare. L'umorismo colto e sagace della storica coppia comica torna sul palco, più forte che mai, con pillole esilaranti di risate concentrate.","gegmen.png", CURRENT_DATE, 1, 1);

INSERT INTO `evento`(`titoloevento`, `artistaevento`, `luogoevento`, `cittaevento`, `dataevento`, `oraevento`, `numeroposti`, `costoevento`, `infoevento`, `imgevento`, `datainserimento`, `organizzatore`, `categoria`) VALUES ("Stasera gioco in casa", "Gianni Morandi", "Teatro Duse", "Bologna", "2020-03-05", "21:00", 50, 40.00,"Il programma potrebbe subire variazioni, si consiglia di fare sempre riferimento alle comunicazioni ufficiali diffuse dall'Organizzatore","morandi.png", CURRENT_DATE, 1, 2);

INSERT INTO `evento`(`titoloevento`, `artistaevento`, `luogoevento`, `cittaevento`, `dataevento`, `oraevento`, `numeroposti`, `costoevento`, `infoevento`, `imgevento`, `datainserimento`, `organizzatore`, `categoria`) VALUES ("Joe Bastianich", "Joe Bastianich", "Eco Teatro", "Milano", "2020-03-05", "20:40", 30, 45.00,"I due live saranno degli show speciali dove verrà approfondita non solo l’anima di Joe Bastianich musicista, ma anche la sua parte più intima: il racconto della sua storia personale anche attraverso la Sua nuova avventura discografica, l’album Aka Joe, pubblicato lo scorso 20 settembre. Il lavoro, registrato a Los Angeles, esplora diversi generi quali alternative rock, alternative country, American music e trova spazio nel panorama musicale contemporaneo, pur proponendo delle sonorità legate al rock blues e al funky. Scrivere canzoni è una terapia per Bastianich, che ha dichiarato la presenza dei suoi ricordi nelle tracce: La musica per me rappresenta l’espressione più pura, l’emozione più vicina al cuore. In questo disco ho raccolto l’essenza più vera della mia vita, quella più intima, tra passioni, paure, ambizioni, amore. Racconta molto di me, di ciò che sono, che ho fatto e che farò. È la mia rivelazione più personale e inedita. La cover dell’album è stata realizzata dall’artista di strada Tvboy (Salvatore Benintende), noto esponente del movimento NeoPop","joe.png", "2019-11-15", 2, 2);

INSERT INTO `evento`(`titoloevento`, `artistaevento`, `luogoevento`, `cittaevento`, `dataevento`, `oraevento`, `numeroposti`, `costoevento`, `infoevento`, `imgevento`, `datainserimento`, `organizzatore`, `categoria`) VALUES ("Green day tour", "green day", "Visano Arena", "Firenze", "2020-06-11", "21:00", 50, 70.00,"Il programma potrebbe subire variazioni, si consiglia di fare sempre riferimento alle comunicazioni ufficiali diffuse dall'Organizzatore","green.png", CURRENT_DATE , 2, 2);

INSERT INTO `evento`(`titoloevento`, `artistaevento`, `luogoevento`, `cittaevento`, `dataevento`, `oraevento`, `numeroposti`, `costoevento`, `infoevento`, `imgevento`, `datainserimento`, `organizzatore`, `categoria`) VALUES ("Carnevale di fano", "città di Fano", "città di Fano", "Fano", "2020-02-23", "15:00", 50, 10.00,"Il programma potrebbe subire variazioni, si consiglia di fare sempre riferimento alle comunicazioni ufficiali diffuse dall'Organizzatore","fano.png", CURRENT_DATE , 3, 3);

INSERT INTO `evento`(`titoloevento`, `artistaevento`, `luogoevento`, `cittaevento`, `dataevento`, `oraevento`, `numeroposti`, `costoevento`, `infoevento`, `imgevento`, `datainserimento`, `organizzatore`, `categoria`) VALUES ("FOO fighters tour", "Foo fighters", "MIND", "Milano", "2020-06-14", "21:00", 50, 55.00,"Il programma potrebbe subire variazioni, si consiglia di fare sempre riferimento alle comunicazioni ufficiali diffuse dall'Organizzatore","foo.png", CURRENT_DATE , 1, 2);


-- -----------------------------------------------------
-- Inserimento Biglietti
-- ----------------------------------------------------- 
ALTER TABLE `biglietto`
  MODIFY `idbiglietto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (1,1,2,"2019-12-13");
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (1,2,4,"2019-12-13");
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (1,3,6,"2019-12-14");
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (1,4,10,"2019-12-15");
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (1,5,3,"2019-12-16");
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (1,6,4,"2019-12-12");

INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (2,1,3,"2019-10-21");
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (2,3,1,"2019-10-03");
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (2,4,2,"2019-10-11");
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (2,6,2,"2019-10-16");


INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (4,2,2,"2019-09-15");
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (4,1,7,"2019-09-11");
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (4,4,12,"2019-09-03");
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (4,5,4,"2019-09-22");


INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (3,3,5,CURRENT_DATE);
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (3,1,2,CURRENT_DATE);

INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (5,4,5,CURRENT_DATE);
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (5,1,1,CURRENT_DATE);

INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (6,1,3,CURRENT_DATE);

INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (7,1,2,"2019-11-15");
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (7,2,4,"2019-12-15");
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (7,3,4,"2019-11-15");
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (7,4,10,"2019-11-15");
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (7,5,5,"2019-11-16");
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (7,6,4,"2019-11-16");

INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (8,6,4,CURRENT_DATE);
INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (8,1,3,CURRENT_DATE);

INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (9,3,4,CURRENT_DATE);

INSERT INTO `biglietto`(`evento`, `cliente`, `posti`, `databiglietto`) VALUES (10,1,3,CURRENT_DATE);

-- -----------------------------------------------------
-- Inserimento notifiche
-- -----------------------------------------------------    
INSERT INTO `notifica`(`titolo`, `descrizione`, `datanotifica`, `letto`, `credenziali`) VALUES ("Intro alle Tecnologie Web Client Side", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", "2020-01-12",0, "cliente2@starticket.com" );

INSERT INTO `notifica`(`titolo`, `descrizione`, `datanotifica`, `letto`, `credenziali`) VALUES ("Intro alle Tecnologie Web Client", "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
, "2020-01-17",0, "cliente2@starticket.com" );

INSERT INTO `notifica`(`titolo`, `descrizione`, `datanotifica`, `letto`, `credenziali`) VALUES ("Intro alle Tecnologie Web Client", "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
, "2020-01-15",0, "cliente2@starticket.com" );